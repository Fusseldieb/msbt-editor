﻿window.onload = function () {
    monaco.editor.defineTheme('msbt-editor', {
        base: 'vs-dark',
        inherit: true,
        rules: [
            {token: 'delimiter',       foreground: '#808080'},
            {token: 'keyword',         foreground: '#569cd6'},
            {token: 'attribute.name',  foreground: '#9cdcfe'},
            {token: 'attribute.value', foreground: '#ce9178'},
            {token: 'comment',         foreground: '#008000'}
        ],
        colors: {}
    });

    //support for MSBT syntax
    monaco.languages.register({
        id: 'msbt',
        extensions: ['.msbt']
    });
    monaco.languages.setMonarchTokensProvider('msbt', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^%%%+/, 'delimiter', '@header'],
                [/^---+/, 'delimiter', '@message'],
                [/(\{\{)(\s*)([A-Za-z0-9_]+:[A-Za-z0-9_]+|[A-Za-z0-9_]+)/, ['delimiter', '', {token: 'keyword', next: '@function'}]]
            ],
            header: [
                [/([A-Za-z0-9_]+\s*:)(\s*[A-Za-z0-9_\-]*)/, ['delimiter', 'keyword']],
                [/^%%%+/, 'delimiter', '@pop']
            ],
            message: [
                [/([A-Za-z0-9_]+\s*:)(\s*[A-Za-z0-9_\-]*)/, ['delimiter', 'keyword']],
                [/^---+/, 'delimiter', '@pop']
            ],
            function: [
                [/([A-Za-z0-9_]+)(=)("[^"]*")/, ['attribute.name', 'delimiter', 'attribute.value']],
                [/\}\}/, 'delimiter', '@pop']
            ]
        }
    });
    monaco.languages.setLanguageConfiguration('msbt', {
        autoClosingPairs: [
            {open: '{{', close: '}}'},
            {open: '[', close: ']'},
        ],
    });

    //support for MJM syntax
    monaco.languages.register({
        id: 'mfm',
        extensions: ['.mfm']
    });
    monaco.languages.setMonarchTokensProvider('mfm', {
        defaultToken: '',
        ignoreCase: true,
        tokenizer: {
            root: [
                [/^\s*#.*$/, 'comment'],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+-\d+|_|\{[A-Za-z0-9_]+\})(\s*\]\s*)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', {token: 'delimiter', next: '@function'}]],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+-\d+|_|\{[A-Za-z0-9_]+\})(\s*\])(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', 'delimiter', {token: 'attribute.name', next: '@function'}]],
                [/^(\[\s*)(\d+)(\s*,\s*)(\d+|\d+-\d+|_|\{[A-Za-z0-9_]+\})(\s*\]\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['delimiter', 'keyword', 'delimiter', 'keyword', 'delimiter', 'attribute.name', {token: 'comment', next: '@function'}]],
                [/^(map)(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', {token: 'attribute.name', next: '@map'}]],
                [/^(map)(\s+[A-Za-z0-9_]+)(\s+[A-Za-z0-9_]+\s*)$/, ['delimiter', 'attribute.name', {token: 'keyword', next: '@map'}]],
                [/^(map)(\s+[A-Za-z0-9_]+)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['delimiter', 'attribute.name', 'keyword', {token: 'comment', next: '@map'}]]
            ],
            function: [
                [/^\s*#.*$/, 'comment'],
                [/^(\s{2,}(?:[A-Za-z0-9_]+|\{[A-Za-z0-9_]+\})(?:\[\d+\])?\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['keyword', 'attribute.name', 'comment']],
                [/^[^\s]+|\s[^\s].*$/, {token: '@rematch', next: '@pop'}]
            ],
            map: [
                [/^\s*#.*$/, 'comment'],
                [/^(\s{2,}[A-Za-z0-9_]+\s*)((?:\s+[A-Za-z0-9_]+\s*)?)((?:\s+#.*)?)$/, ['keyword', 'attribute.name', 'comment']],
                [/^[^\s]+|\s[^\s].*$/, {token: '@rematch', next: '@pop'}]
            ]
        }
    });
    monaco.languages.setLanguageConfiguration('mfm', {
        autoClosingPairs: [
            {open: '[', close: ']'},
            {open: '{', close: '}'}
        ],
    });
}