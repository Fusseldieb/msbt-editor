﻿using System.Diagnostics.CodeAnalysis;
using System.Text.RegularExpressions;

namespace MsbtEditor;

internal static class RegexExtensions
{
    public static bool IsMatch(this Regex regex, string input, [MaybeNullWhen(false)] out Match match)
    {
        match = regex.Match(input);
        return match.Success;
    }
}