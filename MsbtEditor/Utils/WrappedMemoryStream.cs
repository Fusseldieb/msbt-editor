﻿namespace MsbtEditor;

internal class WrappedMemoryStream : Stream
{
    #region private members
    private readonly MemoryStream _stream;
    private readonly Action<MemoryStream> _onDispose;
    private bool _disposed;
    #endregion

    #region constructors
    public WrappedMemoryStream(Action<MemoryStream> onDispose)
    {
        _stream = new MemoryStream();
        _onDispose = onDispose;
    }

    public WrappedMemoryStream(byte[] buffer, Action<MemoryStream> onDispose) : this(buffer, true, onDispose)
    { }

    public WrappedMemoryStream(byte[] buffer, bool writeable, Action<MemoryStream> onDispose)
    {
        _stream = new MemoryStream(buffer, writeable);
        _onDispose = onDispose;
    }
    #endregion

    #region public properties
    public override bool CanRead => _stream.CanRead;
    public override bool CanSeek => _stream.CanSeek;
    public override bool CanWrite => _stream.CanWrite;
    public override long Length => _stream.Length;
    public override long Position
    {
        get => _stream.Position;
        set => _stream.Position = value;
    }
    #endregion

    #region public methods
    public override void Flush() => _stream.Flush();

    public override int Read(byte[] buffer, int offset, int count) => _stream.Read(buffer, offset, count);

    public override long Seek(long offset, SeekOrigin origin) => _stream.Seek(offset, origin);

    public override void SetLength(long value) => _stream.SetLength(value);

    public override void Write(byte[] buffer, int offset, int count) => _stream.Write(buffer, offset, count);
    #endregion

    #region protected methods
    protected override void Dispose(bool disposing)
    {
        if (!disposing || _disposed) return;

        _onDispose.Invoke(_stream);
        _stream.Dispose();
        _disposed = true;
    }
    #endregion
}