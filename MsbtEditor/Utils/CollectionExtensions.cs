﻿namespace MsbtEditor;

internal static class CollectionExtensions
{
    public static bool TryAdd<T>(this ICollection<T> source, T element, Func<T, bool> predicate)
    {
        foreach (var item in source)
        {
            if (!predicate(item)) return false;
        }

        source.Add(element);
        return true;
    }
}