﻿using System.Text.RegularExpressions;
using Newtonsoft.Json;

namespace MsbtEditor;

internal partial class UpdateService
{
    #region private members
    private const string ReleaseUrl = "https://gitlab.com/api/v4/projects/53883547/releases";
    #endregion

    #region public methods
    public async Task<UpdateInfo?> Check()
    {
        string response;
        try
        {
            using var client = new HttpClient();
            response = await client.GetStringAsync(ReleaseUrl);
        }
        catch
        {
            return null;
        }

        if (string.IsNullOrEmpty(response)) return null;
        var data = JsonConvert.DeserializeObject<IList<UpdateData>>(response);

        var latestEntry = data?.FirstOrDefault();
        if (latestEntry is null || !Version.TryParse(latestEntry.TagName.TrimStart('v') + ".0", out var latestVersion)) return null;

        return new UpdateInfo
        {
            Title = latestEntry.Name,
            Description = LinkRegex().Replace(latestEntry.Description.Replace('`', '"'), string.Empty).Trim(),
            Version = latestVersion,
            Link = $"https://gitlab.com/AeonSake/msbt-editor/-/releases/{latestEntry.TagName}"
        };
    }
    #endregion

    #region helper class
    private class UpdateData
    {
        [JsonProperty("name")]
        public string Name { get; set; } = default!;

        [JsonProperty("tag_name")]
        public string TagName { get; set; } = default!;

        [JsonProperty("description")]
        public string Description { get; set; } = default!;

        [JsonProperty("released_at")]
        public DateTime ReleasedAt { get; set; }
    }

    [GeneratedRegex(@"\[[^\]]+\]\([a-zA-Z0-9/_\.]+\)")]
    private static partial Regex LinkRegex();
    #endregion
}