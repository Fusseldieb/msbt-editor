﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

internal sealed class ContextMenuService
{
    #region public events
    public event EventHandler<ContextMenuArgs>? Opened;

    public event EventHandler? Closed;
    #endregion

    #region public methods
    public void Open(ElementReference reference, IEnumerable<UiAction> actions) => Opened?.Invoke(this, new ContextMenuArgs
    {
        Reference = reference,
        Actions = actions
    });

    public void Close() => Closed?.Invoke(this, EventArgs.Empty);
    #endregion

    public class ContextMenuArgs : EventArgs
    {
        public required ElementReference Reference { get; init; }

        public required IEnumerable<UiAction> Actions { get; init; }
    }
}