﻿namespace MsbtEditor;

public abstract class FileListService(bool selectFirstOnRemove = false)
{
    #region private members
    private readonly Dictionary<string, FileData> _files = [];
    #endregion

    #region public properties
    public int Count => _files.Count;

    public IEnumerable<FileData> Files => _files.Values;

    public FileData? SelectedFile { get; private set; }
    #endregion

    #region public events
    public event EventHandler? FilesChanged;

    public event EventHandler<FileData?>? SelectionChanged;
    #endregion

    #region public methods
    public bool Add(FileData file)
    {
        if (!_files.TryAdd(file.FilePath, file)) return false;

        FilesChanged?.Invoke(this, EventArgs.Empty);
        return true;
    }

    public int Add(IEnumerable<FileData> files)
    {
        var fileCount = 0;
        foreach (var file in files)
        {
            if (!_files.TryAdd(file.FilePath, file)) continue;
            ++fileCount;
        }
        if (fileCount == 0) return 0;

        FilesChanged?.Invoke(this, EventArgs.Empty);

        return fileCount;
    }

    public bool Remove(FileData file)
    {
        if (!_files.Remove(file.FilePath)) return false;

        FilesChanged?.Invoke(this, EventArgs.Empty);

        if (SelectedFile is not null && file.Contains(SelectedFile))
        {
            SelectedFile = selectFirstOnRemove ? _files.Values.FirstOrDefault() : null;
            SelectionChanged?.Invoke(this, SelectedFile);
        }
        return true;
    }

    public void Select(FileData? file)
    {
        SelectedFile = file;
        SelectionChanged?.Invoke(this, file);
    }
    #endregion
}