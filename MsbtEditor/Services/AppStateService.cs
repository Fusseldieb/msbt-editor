﻿using System.Diagnostics;
using System.Text;

namespace MsbtEditor;

internal sealed class AppStateService : IDisposable
{
    #region private members
    private readonly FunctionMapFileListService _functionMapFileListService;
    private readonly BrowserFileListService _browserFileListService;
    private readonly EditorService _editorService;

    private readonly FunctionMap _defaultFunctionMap = FunctionMap.Parse(GetDefaultFunctionMap("Default"));
    private readonly FileDataFactory _fileDataFactory;

    private static readonly string RootPath = Path.GetDirectoryName(Application.ExecutablePath)!;
    private static readonly string MapFolder = Path.Combine(RootPath, "maps");
    private static readonly string[] ZstdDictNames = ["zs.zsdic", "zs.dict"];
    #endregion

    #region constructor
    public AppStateService(FunctionMapFileListService functionMapFileListService, BrowserFileListService msbtFileListService, EditorService editorService)
    {
        _functionMapFileListService = functionMapFileListService;
        _browserFileListService = msbtFileListService;
        _editorService = editorService;

        _functionMapFileListService.SelectionChanged += OnFunctionMapFileListSelectionChanged;
        _browserFileListService.SelectionChanged += OnBrowserFileListSelectionChanged;
        _editorService.ContentLoader = LoadEditorContent;
        _editorService.ShowWhitespace = true;

        Settings = new AppSettings
        {
            ZstdCompressionLevel = 19,
            ZstdDict = null,
            FunctionMap = _defaultFunctionMap
        };

        _fileDataFactory = new FileDataFactory(Settings);

        Directory.CreateDirectory(MapFolder);
    }
    #endregion

    #region public properties
    public AppSettings Settings { get; }

    public Func<string, bool, Task<string[]>> OpenFileDialog { get; set; } = default!;

    public Func<Task<string>> OpenFolderDialog { get; set; } = default!;

    public Func<string, Task<string>> SaveFileDialog { get; set; } = default!;

    public Func<Task<string>> FileNameDialog { get; set; } = default!;

    public Func<string, Task> MessageDialog { get; set; } = default!;

    public Func<string, MessageBoxButtons, Task<DialogResult>> ActionDialog { get; set; } = default!;

    public Action<string> StatusMessage { get; set; } = default!;

    public Action<string> StatusMessage2 { get; set; } = default!;

    public Action OnFocus { get; set; } = default!;
    #endregion

    #region public events
    public event EventHandler<bool>? Loading;

    public event EventHandler? GlossaryToggled;
    #endregion

    #region public methods
    public async Task LoadDefaultFiles()
    {
        foreach (var zstdDict in ZstdDictNames)
        {
            var filePath = Path.Combine(RootPath, zstdDict);
            if (!File.Exists(filePath)) continue;

            Settings.ZstdDict = await File.ReadAllBytesAsync(filePath);
            break;
        }

        var files = new List<FunctionMapFileData>();
        foreach (var filePath in Directory.GetFiles(MapFolder, "*.mfm", SearchOption.AllDirectories))
        {
            try
            {
                files.Add(await LoadFunctionMap(filePath));
            }
            catch
            {
                //ignored
            }
        }

        var addedFiles = _functionMapFileListService.Add(files);
        if (addedFiles > 0) _functionMapFileListService.Select(files[0]);
        StatusMessage($"Loaded {addedFiles} function map{(addedFiles == 1 ? "" : "s")}{(Settings.ZstdDict is not null ? " | Loaded custom ZSTD dictionary": "")}");
    }

    public async Task LoadFiles()
    {
        var filePaths = await OpenFileDialog("All Supported Files|*.msbt;*.sarc;*.sarc.zs;*.umsbt;*.bmg;*.zip" +
                                             "|MSBT Files|*.msbt" +
                                             "|SARC Files|*.sarc" +
                                             "|Compressed SARC Files|*sarc.zs" +
                                             "|UMSBT Files|*.umsbt" +
                                             "|BMG Files|*.bmg" +
                                             "|All Files|*.*", true);
        if (filePaths.Length == 0) return;

        Loading?.Invoke(this, true);
        StatusMessage("Loading...");
        var sw = Stopwatch.StartNew();

        var files = new List<FileData>();
        foreach (var filePath in filePaths)
        {
            var fixedFilePath = filePath.Replace('\\', '/');

            try
            {
                var provider = new PhysicalFileStorageProvider(fixedFilePath);
                var file = _fileDataFactory.Create(provider, fixedFilePath);

                await file.Load();
                files.Add(file);
            }
            catch (Exception ex)
            {
                Loading?.Invoke(this, false);
                StatusMessage("Failed to load file");
                await MessageDialog($"Failed to load \"{filePath}\": {ex.Message}");
                return;
            }
        }

        var addedFiles = _browserFileListService.Add(files);
        if (addedFiles > 0) _browserFileListService.Select(files[0]);

        sw.Stop();
        Loading?.Invoke(this, false);
        StatusMessage($"Loaded {addedFiles} file{(addedFiles == 1 ? "" : "s")} in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task UnloadFile(FileData file)
    {
        if (file.Contains(f => f.IsModified))
        {
            var result = await ActionDialog($"Do you want to save the changes you made to \"{file.Name}\"? Your changes will be lost if you don't save them.", MessageBoxButtons.YesNoCancel);
            switch (result)
            {
                case DialogResult.Cancel:
                    return;
                case DialogResult.Yes:
                    await SaveFile(file);
                    break;
            }
        }

        await UnloadChildFiles(file);
        _browserFileListService.Remove(file);
        _functionMapFileListService.Remove(file);
        return;

        async Task UnloadChildFiles(FileData childFile)
        {
            childFile.IsModified = false;
            if (file is FunctionMapFileData) file.Modified -= OnFunctionMapFileModified;

            if (childFile is EditorFileData editorFile)
            {
                if (_editorService.CurrentFile == editorFile) await _editorService.Load(null);
                await _editorService.Unload(editorFile);
            }

            foreach (var childFile2 in childFile.Children)
            {
                await UnloadChildFiles(childFile2);
            }
        }
    }

    public async Task LoadFolder()
    {
        var folderPath = await OpenFolderDialog();
        if (string.IsNullOrEmpty(folderPath) || !Directory.Exists(folderPath)) return;

        var fixedFolderPath = folderPath.Replace('\\', '/');
        Loading?.Invoke(this, true);
        StatusMessage("Loading...");
        var sw = Stopwatch.StartNew();

        try
        {
            var file = new FolderFileData(_fileDataFactory) {FilePath = fixedFolderPath};
            await file.Load();
            _browserFileListService.Add(file);
        }
        catch (Exception ex)
        {
            Loading?.Invoke(this, false);
            StatusMessage("Failed to load folder");
            await MessageDialog($"Failed to load \"{folderPath}\": {ex.Message}");
            return;
        }

        sw.Stop();
        Loading?.Invoke(this, false);
        StatusMessage($"Loaded folder content in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task LoadFunctionMaps()
    {
        var filePaths = await OpenFileDialog("Function Maps|*.mfm", true);
        if (filePaths.Length == 0) return;

        Loading?.Invoke(this, true);
        StatusMessage("Loading...");
        var sw = Stopwatch.StartNew();

        var files = new List<FunctionMapFileData>();
        foreach (var filePath in filePaths)
        {
            try
            {
                files.Add(await LoadFunctionMap(filePath));
            }
            catch (Exception ex)
            {
                Loading?.Invoke(this, false);
                StatusMessage("Failed to load function map");
                await MessageDialog($"Failed to load \"{filePath}\": {ex.Message}");
                return;
            }
        }

        var addedFiles = _functionMapFileListService.Add(files);
        if (addedFiles > 0) _functionMapFileListService.Select(files[0]);

        sw.Stop();
        Loading?.Invoke(this, false);
        StatusMessage($"Loaded {addedFiles} function map{(addedFiles == 1 ? "" : "s")} in {sw.Elapsed.TotalSeconds:F2}s");
    }

    public async Task CreateFunctionMap()
    {
        var fileName = await FileNameDialog();
        if (string.IsNullOrEmpty(fileName)) return;

        var filePath = Path.Combine(MapFolder, fileName + ".mfm");
        if (File.Exists(filePath))
        {
            await MessageDialog("A function map with the same name already exists.");
            return;
        }

        var content = GetDefaultFunctionMap(fileName);

        try
        {
            await File.WriteAllTextAsync(filePath, content, Encoding.UTF8);
        }
        catch (Exception ex)
        {
            await MessageDialog($"Failed to create \"{fileName}\": {ex.Message}");
            return;
        }

        var file = await LoadFunctionMap(filePath);
        _functionMapFileListService.Add(file);
        await _editorService.Load(file);
        _functionMapFileListService.Select(file);
    }

    public async Task LoadZstdDict()
    {
        var filePaths = await OpenFileDialog("All Files|*.*", false);
        if (filePaths.Length == 0) return;

        StatusMessage("Loading...");

        try
        {
            Settings.ZstdDict = await File.ReadAllBytesAsync(filePaths[0]);
            StatusMessage("Loaded custom ZSTD dictionary");
        }
        catch (Exception ex)
        {
            StatusMessage("Failed to load ZSTD dictionary");
            await MessageDialog($"Failed to load \"{filePaths[0]}\": {ex.Message}");
        }
    }

    public Task UnloadZstdDict()
    {
        Settings.ZstdDict = null;
        StatusMessage("Unloaded custom ZSTD dictionary");
        return Task.CompletedTask;
    }

    public async Task SaveFile(FileData file)
    {
        if (!file.IsModified) return;

        Loading?.Invoke(this, true);
        StatusMessage("Saving...");
        var sw = Stopwatch.StartNew();

        try
        {
            await file.Save();

            sw.Stop();
            Loading?.Invoke(this, false);
            StatusMessage($"File saved in {sw.Elapsed.TotalSeconds:F2}s");
        }
        catch (Exception ex)
        {
            Loading?.Invoke(this, false);
            StatusMessage("Failed to save file");
            await MessageDialog($"Failed to save \"{file.Name}\": {ex.Message}");
        }
    }

    public async Task SaveAllFiles()
    {
        Loading?.Invoke(this, true);
        StatusMessage("Saving...");
        var sw = Stopwatch.StartNew();

        try
        {
            foreach (var file in _functionMapFileListService.Files) await file.Save();
            foreach (var file in _browserFileListService.Files) await file.Save();

            sw.Stop();
            Loading?.Invoke(this, false);
            StatusMessage($"Files saved in {sw.Elapsed.TotalSeconds:F2}s");
        }
        catch (Exception ex)
        {
            Loading?.Invoke(this, false);
            StatusMessage("Failed to save file");
            await MessageDialog($"Failed to save a file: {ex.Message}");
        }
    }

    public Task SaveCurrentEditorFile()
    {
        var file = _editorService.CurrentFile;
        return file is null ? Task.CompletedTask : SaveFile(file);
    }

    public async Task SaveFileAs(FileData file, bool raw = false)
    {
        var filePath = file.FilePath;
        if (raw && filePath.EndsWith(".zs")) filePath = filePath[..^3];

        filePath = await SaveFileDialog(filePath);
        if (string.IsNullOrEmpty(filePath)) return;

        Loading?.Invoke(this, true);
        StatusMessage("Saving...");
        var sw = Stopwatch.StartNew();

        try
        {
            await using var stream = File.Create(filePath);
            await file.Save(stream);

            sw.Stop();
            Loading?.Invoke(this, false);
            StatusMessage($"File saved in {sw.Elapsed.TotalSeconds:F2}s");
        }
        catch (Exception ex)
        {
            Loading?.Invoke(this, false);
            StatusMessage("Failed to save file");
            await MessageDialog($"Failed to save \"{file.Name}\": {ex.Message}");
        }
    }

    public async Task RevertChanges(FileData file)
    {
        if (!file.IsModified) return;

        var result = await ActionDialog($"Are you sure you want to revert changes in \"{file.Name}\"?", MessageBoxButtons.OKCancel);
        if (result != DialogResult.OK) return;

        Loading?.Invoke(this, true);
        StatusMessage($"Reverting changes on \"{file.Name}\"...");
        var sw = Stopwatch.StartNew();

        try
        {
            var selectedFile = _browserFileListService.SelectedFile;
            var editorFile = _editorService.CurrentFile;

            await file.Load();

            if (selectedFile is not null)
            {
                var foundFile = file.Find(f => f.FilePath.Equals(selectedFile.FilePath));
                _browserFileListService.Select(foundFile);
            }
            if (editorFile is not null)
            {
                var foundFile = file.Find<EditorFileData>(f => f.FilePath.Equals(editorFile.FilePath));
                await _editorService.Load(foundFile);
            }

            sw.Stop();
            Loading?.Invoke(this, false);
            StatusMessage($"Changes reverted in {sw.Elapsed.TotalSeconds:F2}s");
        }
        catch (Exception ex)
        {
            Loading?.Invoke(this, false);
            await MessageDialog($"Failed to revert changes on \"{file.Name}\": {ex.Message}");
        }
    }

    public async Task DeleteFile(FileData file)
    {
        if (file.IsVirtual) return;

        var result = await ActionDialog($"Are you sure you want to delete \"{file.Name}\"? This action cannot be undone.", MessageBoxButtons.YesNo);
        if (result != DialogResult.Yes) return;

        try
        {
            if (file is FolderFileData)
            {
                Directory.Delete(file.FilePath, true);
                return;
            }
            File.Delete(file.FilePath);
        }
        catch (Exception ex)
        {
            await MessageDialog($"Failed to delete \"{file.Name}\": {ex.Message}");
            return;
        }

        if (file is EditorFileData editorFile)
        {
            if (_editorService.CurrentFile == editorFile) await _editorService.Load(null);
            await _editorService.Unload(editorFile);
        }
        if (file is FunctionMapFileData) file.Modified -= OnFunctionMapFileModified;

        _browserFileListService.Remove(file);
        _functionMapFileListService.Remove(file);
    }

    public bool HasModifiedFile() => _browserFileListService.Files.Any(file => file.Contains(f => f.IsModified)) || _functionMapFileListService.Files.Any(file => file.IsModified);

    public void ToggleFunctionGlossary() => GlossaryToggled?.Invoke(this, EventArgs.Empty);

    public void ToggleWordWrap() => _editorService.WordWrap = !_editorService.WordWrap;

    public void ToggleShowNewLine() => _editorService.ShowNewLine = !_editorService.ShowNewLine;

    public void ToggleShowWhitespace() => _editorService.ShowWhitespace = !_editorService.ShowWhitespace;
    #endregion

    #region private methods
    private async Task<FunctionMapFileData> LoadFunctionMap(string filePath)
    {
        var fixedFilePath = filePath.Replace('\\', '/');

        var file = new FunctionMapFileData(new PhysicalFileStorageProvider(fixedFilePath))
        {
            FilePath = fixedFilePath
        };
        file.Modified += OnFunctionMapFileModified;

        await file.Load();
        return file;
    }

    private static string GetDefaultFunctionMap(string name)
    {
        return $"""
                ### {name} Function Map

                #System group
                [0, 0] ruby #ruby annotation rendered above the normal text
                  u16 charSpan #number of subsequent characters to render the annotation above (centered)
                  str value #the annotation text
                [0, 2] size #changes the current text size
                  u16 value #relative text size in percent
                [0, 3] color #changes the current text color
                  s16 id #id of the color
                [0, 4] pageBreak #forces a new dialog page

                """;
    }

    private async Task SelectFunctionMap(FunctionMapFileData? file)
    {
        if (file?.Model == Settings.FunctionMap) return;

        Settings.FunctionMap = file?.Model ?? _defaultFunctionMap;

        foreach (var browserFile in _browserFileListService.Files)
        {
            await UpdateFile(browserFile);
        }

        StatusMessage2("Function map: " + (file?.Model is null ? "(default)" : file.Name));
        return;

        async Task UpdateFile(FileData browserFile)
        {
            if (browserFile is EditorFileData {EditorLanguage: "msbt"} editorFile)
            {
                try
                {
                    await editorFile.LoadEditorContent();
                }
                catch (Exception ex)
                {
                    await MessageDialog($"Failed to load editor content: {ex.Message}");
                    return;
                }
            }

            foreach (var childFile in browserFile.Children)
            {
                await UpdateFile(childFile);
            }
        }
    }

    private async Task LoadEditorContent(EditorFileData file)
    {
        try
        {
            await file.LoadEditorContent();
        }
        catch (Exception ex)
        {
            await MessageDialog($"Failed to load editor content: {ex.Message}");
        }
    }

    private async void OnFunctionMapFileListSelectionChanged(object? sender, FileData? file) => await SelectFunctionMap(file as FunctionMapFileData);

    private async void OnBrowserFileListSelectionChanged(object? sender, FileData? file)
    {
        if (file is null) return;

        try
        {
            if (!file.IsInitialized) await file.Load();
        }
        catch (Exception ex)
        {
            await MessageDialog($"Failed to load \"{file.Name}\": {ex.Message}");
            return;
        }

        if (file is not EditorFileData editorFile || editorFile == _editorService.CurrentFile) return;
        await _editorService.Load(editorFile);
    }

    private async void OnFunctionMapFileModified(object? sender, EventArgs args)
    {
        if (sender is not FunctionMapFileData file || file.IsModified) return;
        await SelectFunctionMap(file);
    }
    #endregion

    #region IDisposable interface
    public void Dispose()
    {
        _functionMapFileListService.SelectionChanged -= OnFunctionMapFileListSelectionChanged;
        _browserFileListService.SelectionChanged -= OnBrowserFileListSelectionChanged;
        foreach (var file in _functionMapFileListService.Files) file.Modified -= OnFunctionMapFileModified;
        _editorService.ContentLoader = _ => Task.FromResult(string.Empty);
    }
    #endregion
}