﻿using BlazorMonaco.Editor;

namespace MsbtEditor;

internal sealed class EditorService
{
    #region private members
    private readonly Dictionary<EditorFileData, TextModel> _models = [];
    private bool _wordWrap;
    private bool _showNewLine;
    private bool _showWhitespace;
    #endregion

    #region public properties
    public IEnumerable<EditorFileData> Files => _models.Keys;

    public EditorFileData? CurrentFile { get; private set; }

    public Func<EditorFileData, Task> ContentLoader { get; set; } = _ => Task.CompletedTask;

    public bool WordWrap
    {
        get => _wordWrap;
        set
        {
            if (_wordWrap == value) return;
            _wordWrap = value;
            WordWrapChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public bool ShowNewLine
    {
        get => _showNewLine;
        set
        {
            if (_showNewLine == value) return;
            _showNewLine = value;
            ShowNewLineChanged?.Invoke(this, EventArgs.Empty);
        }
    }

    public bool ShowWhitespace
    {
        get => _showWhitespace;
        set
        {
            if (_showWhitespace == value) return;
            _showWhitespace = value;
            ShowWhitespaceChanged?.Invoke(this, EventArgs.Empty);
        }
    }
    #endregion

    #region public events
    public event EventHandler<EditorFileData?>? CurrentFileChanged;

    public event EventHandler? WordWrapChanged;

    public event EventHandler? ShowNewLineChanged;

    public event EventHandler? ShowWhitespaceChanged;
    #endregion

    #region public methods
    public async Task Load(EditorFileData? file)
    {
        var oldFile = CurrentFile;
        CurrentFile = file;
        if (file is not null) file.Modified += OnFileModified;

        //same URI -> model was reloaded and file instance changed
        if (oldFile is not null && oldFile.FilePath == file?.FilePath)
        {
            file.EditorModel = oldFile.EditorModel;
            await ContentLoader(file);
        }

        CurrentFileChanged?.Invoke(this, file);
        if (oldFile is not null) await DisposeIfUnused(oldFile);
    }

    public Task Unload(EditorFileData file)
    {
        if (file == CurrentFile)
        {
            CurrentFile = null;
            CurrentFileChanged?.Invoke(this, null);
        }

        file.Modified -= OnFileModified;
        if (file.EditorModel is null) return Task.CompletedTask;

        var model = file.EditorModel;
        file.EditorModel = null;
        return model.DisposeModel();
    }
    #endregion

    #region private methods
    private async void OnFileModified(object? sender, EventArgs args)
    {
        if (sender is EditorFileData file) await DisposeIfUnused(file);
    }

    private Task DisposeIfUnused(EditorFileData file)
    {
        if (file.IsModified || file == CurrentFile) return Task.CompletedTask;

        file.Modified -= OnFileModified;
        if (file.EditorModel is null) return Task.CompletedTask;

        var model = file.EditorModel;
        file.EditorModel = null;
        return model.DisposeModel();
    }
    #endregion
}