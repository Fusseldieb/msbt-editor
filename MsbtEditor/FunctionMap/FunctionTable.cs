﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace MsbtEditor;

internal class FunctionTable(FunctionMap map) : IMsbtFunctionTable
{
    public bool BigEndian { get; set; }

    public void GetFunction(MsbtFunction function, Encoding encoding, out string functionName, out IEnumerable<MsbtFunctionArgument> functionArgs)
    {
        var argList = new List<MsbtFunctionArgument>();
        var argOffset = 0;

        if (map.TryGetFunction(function.Group, function.Type, out var functionInfo))
        {
            functionName = functionInfo.Name;
            if (functionInfo.IsDiscardType || functionInfo.TypeRange > 0) functionName += $":{function.Type}";
            else if (functionInfo.TypeMap is not null)
            {
                var searchType = function.Type.ToString();
                var value = Array.Find(functionInfo.TypeMap, v => v.Value == searchType) ??
                            throw new FileFormatException($"Failed to parse function type of \"{functionName}\" from map.");
                functionName += $":{value.Name}";
            }

            foreach (var arg in functionInfo.Args)
            {
                //discard padding
                if (arg.IsDiscard)
                {
                    argOffset = function.Args.Length;
                    break;
                }

                //argument padding
                if (arg.IsPadding)
                {
                    argOffset += arg.DataType.Length * (arg.ArrayLength > 0 ? arg.ArrayLength : 1);
                    continue;
                }

                //handle arrays
                if (arg.ArrayLength > 0)
                {
                    var arr = new object[arg.ArrayLength];
                    for (var i = 0; i < arg.ArrayLength; ++i)
                    {
                        arr[i] = ParseArgument(arg, functionInfo);
                    }
                    argList.Add(new MsbtFunctionArgument(arg.Name, arr));
                }
                else
                {
                    argList.Add(new MsbtFunctionArgument(arg.Name, ParseArgument(arg, functionInfo)));
                }
            }

            if (argOffset < function.Args.Length)
            {
                argList.Add(new MsbtFunctionArgument("otherArg", function.Args[argOffset..].ToHexString(true)));
            }
        }
        else
        {
            functionName = $"{function.Group}:{function.Type}";
            if (function.Args.Length > 0) argList.Add(new MsbtFunctionArgument("arg", function.Args.ToHexString(true)));
        }

        functionArgs = argList;
        return;

        string ParseArgument(FunctionArg arg, FunctionInfo info)
        {
            try
            {
                var (value, count) = arg.DataType.Deserialize(function.Args, argOffset, BigEndian, encoding);
                argOffset += count;

                if (arg.ValueMap is null) return value;
                foreach (var mapValue in arg.ValueMap)
                {
                    if (mapValue.Value == value) return mapValue.Name;
                }
                return value;
            }
            catch
            {
                throw new FileFormatException($"Failed to parse function argument value of \"{arg.Name}\" on \"{info.Name}\" as {arg.DataType.Name}.");
            }
        }
    }
}