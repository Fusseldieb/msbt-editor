using System.Globalization;
using Microsoft.Extensions.DependencyInjection;

namespace MsbtEditor;

internal static class Program
{
    [STAThread]
    private static void Main()
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        var services = new ServiceCollection();
        services.AddSingleton<BrowserFileListService>();
        services.AddSingleton<FunctionMapFileListService>();
        services.AddSingleton<EditorService>();
        services.AddSingleton<ContextMenuService>();
        services.AddSingleton<AppStateService>();
        services.AddSingleton<UpdateService>();
        services.AddWindowsFormsBlazorWebView();
        var serviceProvider = services.BuildServiceProvider();

        ApplicationConfiguration.Initialize();
        Application.Run(new MainForm(serviceProvider));
    }
}