﻿using System.Reflection;

namespace MsbtEditor;

public partial class AboutDialog : Form
{
    public AboutDialog()
    {
        InitializeComponent();

        richTextBox1.SelectionProtected = true;
        richTextBox1.Text = $"""
                             An interactive editor for MSBT files. Allows opening, editing and saving MSBT files and provides syntax highlighting with customizable function maps.

                             This project is open-source and available under: https://gitlab.com/AeonSake/msbt-editor
                             NintendoTools is used for processing Nintendo file formats: https://gitlab.com/AeonSake/nintendo-tools

                             Version: {Assembly.GetExecutingAssembly().GetName().Version}
                             {Assembly.GetExecutingAssembly().GetCustomAttributes<AssemblyCopyrightAttribute>().First().Copyright}
                             """;
    }

    private void ButtonOk_Click(object sender, EventArgs e) => Close();
}