using System.Reflection;
using Microsoft.AspNetCore.Components.WebView;
using Microsoft.AspNetCore.Components.WebView.WindowsForms;
using Microsoft.Extensions.DependencyInjection;
using MsbtEditor.Components;

namespace MsbtEditor;

public partial class MainForm : Form
{
    #region private members
    private readonly AppStateService _appStateService;
    private readonly UpdateService _updateService;
    private readonly double[] _zoomLevels = [0.25, 0.33, 0.5, 0.66, 0.75, 0.8, 0.9, 1, 1.1, 1.25, 1.5, 1.75, 2, 2.5, 3, 4, 5];
    private const int DefaultZoomIndex = 7;
    private int _zoomIndex = DefaultZoomIndex;
    #endregion

    #region constructor
    public MainForm(IServiceProvider serviceProvider)
    {
        _appStateService = serviceProvider.GetRequiredService<AppStateService>();
        _appStateService.OpenFileDialog = OpenFileDialog;
        _appStateService.OpenFolderDialog = OpenFolderDialog;
        _appStateService.SaveFileDialog = SaveFileDialog;
        _appStateService.FileNameDialog = FileNameDialog;
        _appStateService.MessageDialog = MessageDialog;
        _appStateService.ActionDialog = ActionDialog;
        _appStateService.StatusMessage = StatusMessage;
        _appStateService.StatusMessage2 = StatusMessage2;
        _appStateService.OnFocus = () =>
        {
            if (ActiveControl != blazorWebView) ActiveControl = blazorWebView;
            menuItemFile.HideDropDown();
            menuItemView.HideDropDown();
        };

        _updateService = serviceProvider.GetRequiredService<UpdateService>();

        InitializeComponent();
        menuStrip.Renderer = new ToolStripRenderer();

        blazorWebView.HostPage = "wwwroot\\index.html";
        blazorWebView.Services = serviceProvider;
        blazorWebView.RootComponents.Add<MainLayout>("#app");
        blazorWebView.WebView.DefaultBackgroundColor = Color.FromArgb(0xff, 0x25, 0x25, 0x26);
        blazorWebView.WebView.ZoomFactorChanged += BlazorWebView_ZoomFactorChanged;
        blazorWebView.BlazorWebViewInitialized += BlazorWebView_Initialized;

        #if DEBUG
        toolStripSeparator4.Visible = true;
        toolStripMenuDevTools.Visible = true;
        toolStripMenuTaskManager.Visible = true;
        #endif
    }
    #endregion

    #region ui handlers
    private Task<string[]> OpenFileDialog(string filter, bool multiSelect) => Task.Run(() =>
    {
        var dialog = new OpenFileDialog
        {
            Filter = filter,
            Multiselect = multiSelect
        };

        return InvokeIfRequired(() => dialog.ShowDialog(this) == DialogResult.OK ? dialog.FileNames : []);
    });

    private Task<string> OpenFolderDialog() => Task.Run(() =>
    {
        var dialog = new FolderBrowserDialog();

        return InvokeIfRequired(() => dialog.ShowDialog(this) == DialogResult.OK ? dialog.SelectedPath : string.Empty);
    });

    private Task<string> SaveFileDialog(string fileName) => Task.Run(() =>
    {
        var dialog = new SaveFileDialog
        {
            CheckFileExists = true,
            FileName = fileName
        };

        return InvokeIfRequired(() => dialog.ShowDialog(this) == DialogResult.OK ? dialog.FileName : string.Empty);
    });

    private Task<string> FileNameDialog() => Task.Run(() =>
    {
        var dialog = new FileNameDialog();

        return InvokeIfRequired(() => dialog.ShowDialog(this) == DialogResult.OK ? dialog.FileName : string.Empty);
    });

    private Task<DialogResult> MessageDialog(string message) => ActionDialog(message, MessageBoxButtons.OK);

    private Task<DialogResult> ActionDialog(string message, MessageBoxButtons buttons) => Task.Run(() =>
    {
        return InvokeIfRequired(() => MessageBox.Show(this, message, "MSBT Editor", buttons));
    });

    private void StatusMessage(string message) => InvokeIfRequired(() => statusMessage.Text = message);

    private void StatusMessage2(string message) => InvokeIfRequired(() => statusMessage2.Text = message);

    private T InvokeIfRequired<T>(Func<T> action) => InvokeRequired ? Invoke(action) : action();
    #endregion

    #region form events
    private async void MainForm_Load(object sender, EventArgs e)
    {
        await _appStateService.LoadDefaultFiles();
        toolStripMenuUnloadZstdDict.Enabled = _appStateService.Settings.ZstdDict is not null;
    }

    private async void MainForm_FormClosing(object sender, FormClosingEventArgs e)
    {
        if (e.CloseReason != CloseReason.UserClosing) return;
        if (!_appStateService.HasModifiedFile()) return;

        switch (MessageBox.Show("You have unsaved changes. Your changes will be lost if you don't save them. Save changes?", "MSBT Editor", MessageBoxButtons.YesNoCancel))
        {
            case DialogResult.Yes:
                e.Cancel = true;
                await _appStateService.SaveAllFiles();
                Close();
                break;
            case DialogResult.Cancel:
                e.Cancel = true;
                break;
        }
    }

    private void BlazorWebView_Initialized(object? sender, BlazorWebViewInitializedEventArgs e)
    {
        toolStripMenuDevTools.Enabled = true;
        toolStripMenuTaskManager.Enabled = true;
    }

    private async void ToolStripLoadFiles_Click(object sender, EventArgs e) => await _appStateService.LoadFiles();

    private async void ToolStripLoadFolder_Click(object sender, EventArgs e) => await _appStateService.LoadFolder();

    private async void ToolStripMenuLoadFunctionMap_Click(object sender, EventArgs e) => await _appStateService.LoadFunctionMaps();

    private async void ToolStripMenuLoadZstdDict_Click(object sender, EventArgs e)
    {
        await _appStateService.LoadZstdDict();
        toolStripMenuUnloadZstdDict.Enabled = _appStateService.Settings.ZstdDict is not null;
    }

    private async void ToolStripMenuUnloadZstdDict_Click(object sender, EventArgs e)
    {
        await _appStateService.UnloadZstdDict();
        toolStripMenuUnloadZstdDict.Enabled = false;
    }

    private async void ToolStripMenuSave_Click(object sender, EventArgs e) => await _appStateService.SaveCurrentEditorFile();

    private async void ToolStripMenuSaveAll_Click(object sender, EventArgs e) => await _appStateService.SaveAllFiles();

    private void ToolStripMenuAbout_Click(object sender, EventArgs e) => new AboutDialog().ShowDialog(this);

    private async void ToolStripMenuUpdate_Click(object sender, EventArgs e)
    {
        toolStripMenuUpdate.Enabled = false;
        var info = await _updateService.Check();
        toolStripMenuUpdate.Enabled = true;

        if (info is null)
        {
            await MessageDialog("Failed to check for update. Make sure you are connected to the internet.");
        }
        else if (info.Version.CompareTo(Assembly.GetExecutingAssembly().GetName().Version) > 0)
        {
            var dialog = new UpdateDialog(info);
            InvokeIfRequired(() => dialog.ShowDialog(this));
        }
        else
        {
            await MessageDialog("You are using the latest version.");
        }
    }

    private void ToolStripMenuIExit_Click(object sender, EventArgs e) => Close();

    private void ToolStripMenuZoomIn_Click(object sender, EventArgs e)
    {
        if (_zoomIndex + 1 >= _zoomLevels.Length) return;

        blazorWebView.WebView.ZoomFactor = _zoomLevels[++_zoomIndex];
        UpdateZoomControls();
    }

    private void ToolStripMenuZoomOut_Click(object sender, EventArgs e)
    {
        if (_zoomIndex <= 0) return;

        blazorWebView.WebView.ZoomFactor = _zoomLevels[--_zoomIndex];
        UpdateZoomControls();
    }

    private void ToolStripMenuZoomReset_Click(object sender, EventArgs e)
    {
        _zoomIndex = DefaultZoomIndex;
        blazorWebView.WebView.ZoomFactor = _zoomLevels[_zoomIndex];
        UpdateZoomControls();
    }

    private void BlazorWebView_ZoomFactorChanged(object? sender, EventArgs e)
    {
        for (var i = 0; i < _zoomLevels.Length; ++i)
        {
            if (Math.Abs(blazorWebView.WebView.ZoomFactor - _zoomLevels[i]) > 0.0001) continue;

            _zoomIndex = i;
            UpdateZoomControls();
            return;
        }
    }

    private void UpdateZoomControls()
    {
        toolStripMenuZoomIn.Enabled = _zoomIndex + 1 < _zoomLevels.Length;
        toolStripMenuZoomOut.Enabled = _zoomIndex > 0;
        toolStripMenuZoomReset.Enabled = _zoomIndex != DefaultZoomIndex;
    }

    private void ToolStripMenuGlossary_Click(object sender, EventArgs e) => _appStateService.ToggleFunctionGlossary();

    private void ToolStripMenuDevTools_Click(object sender, EventArgs e) => blazorWebView.WebView.CoreWebView2.OpenDevToolsWindow();

    private void ToolStripMenuTaskManager_Click(object sender, EventArgs e) => blazorWebView.WebView.CoreWebView2.OpenTaskManagerWindow();

    private void ToolStripMenuWordWrap_Click(object sender, EventArgs e) => _appStateService.ToggleWordWrap();

    private void ToolStripMenuNewLine_Click(object sender, EventArgs e) => _appStateService.ToggleShowNewLine();

    private void ToolStripMenuWhiteSpace_Click(object sender, EventArgs e) => _appStateService.ToggleShowWhitespace();

    #endregion
}