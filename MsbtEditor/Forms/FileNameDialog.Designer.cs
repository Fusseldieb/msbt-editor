﻿namespace MsbtEditor;

partial class FileNameDialog
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        flowLayoutPanel1 = new FlowLayoutPanel();
        buttonCancel = new Button();
        buttonOk = new Button();
        textBoxName = new TextBox();
        labelInfo = new Label();
        panel1 = new Panel();
        flowLayoutPanel1.SuspendLayout();
        panel1.SuspendLayout();
        SuspendLayout();
        // 
        // flowLayoutPanel1
        // 
        flowLayoutPanel1.AutoSize = true;
        flowLayoutPanel1.Controls.Add(buttonCancel);
        flowLayoutPanel1.Controls.Add(buttonOk);
        flowLayoutPanel1.Dock = DockStyle.Bottom;
        flowLayoutPanel1.FlowDirection = FlowDirection.RightToLeft;
        flowLayoutPanel1.Location = new Point(5, 77);
        flowLayoutPanel1.Margin = new Padding(0);
        flowLayoutPanel1.Name = "flowLayoutPanel1";
        flowLayoutPanel1.Size = new Size(234, 29);
        flowLayoutPanel1.TabIndex = 2;
        // 
        // buttonCancel
        // 
        buttonCancel.BackColor = Color.FromArgb(75, 75, 75);
        buttonCancel.DialogResult = DialogResult.Cancel;
        buttonCancel.FlatStyle = FlatStyle.Flat;
        buttonCancel.Location = new Point(156, 3);
        buttonCancel.Name = "buttonCancel";
        buttonCancel.Size = new Size(75, 23);
        buttonCancel.TabIndex = 1;
        buttonCancel.Text = "Cancel";
        buttonCancel.UseVisualStyleBackColor = false;
        // 
        // buttonOk
        // 
        buttonOk.BackColor = Color.FromArgb(75, 75, 75);
        buttonOk.DialogResult = DialogResult.OK;
        buttonOk.Enabled = false;
        buttonOk.FlatStyle = FlatStyle.Flat;
        buttonOk.Location = new Point(75, 3);
        buttonOk.Name = "buttonOk";
        buttonOk.Size = new Size(75, 23);
        buttonOk.TabIndex = 0;
        buttonOk.Text = "OK";
        buttonOk.UseVisualStyleBackColor = false;
        // 
        // textBoxName
        // 
        textBoxName.BackColor = Color.FromArgb(75, 75, 75);
        textBoxName.BorderStyle = BorderStyle.FixedSingle;
        textBoxName.Dock = DockStyle.Fill;
        textBoxName.ForeColor = Color.White;
        textBoxName.Location = new Point(20, 0);
        textBoxName.Margin = new Padding(0);
        textBoxName.MaxLength = 100;
        textBoxName.Name = "textBoxName";
        textBoxName.Size = new Size(194, 23);
        textBoxName.TabIndex = 1;
        textBoxName.TextChanged += TextBoxName_TextChanged;
        // 
        // labelInfo
        // 
        labelInfo.Dock = DockStyle.Top;
        labelInfo.Location = new Point(5, 5);
        labelInfo.Name = "labelInfo";
        labelInfo.Size = new Size(234, 34);
        labelInfo.TabIndex = 0;
        labelInfo.Text = "Choose a name for the file to create:";
        labelInfo.TextAlign = ContentAlignment.MiddleCenter;
        // 
        // panel1
        // 
        panel1.Controls.Add(textBoxName);
        panel1.Dock = DockStyle.Fill;
        panel1.Location = new Point(5, 39);
        panel1.Margin = new Padding(0);
        panel1.Name = "panel1";
        panel1.Padding = new Padding(20, 0, 20, 0);
        panel1.Size = new Size(234, 38);
        panel1.TabIndex = 3;
        // 
        // FileNameDialog
        // 
        AcceptButton = buttonOk;
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(51, 51, 51);
        CancelButton = buttonCancel;
        ClientSize = new Size(244, 111);
        Controls.Add(panel1);
        Controls.Add(labelInfo);
        Controls.Add(flowLayoutPanel1);
        ForeColor = Color.FromArgb(212, 212, 212);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "FileNameDialog";
        Padding = new Padding(5);
        ShowIcon = false;
        StartPosition = FormStartPosition.CenterParent;
        Text = "Select File Name";
        flowLayoutPanel1.ResumeLayout(false);
        panel1.ResumeLayout(false);
        panel1.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private FlowLayoutPanel flowLayoutPanel1;
    private Button buttonOk;
    private Button buttonCancel;
    private TextBox textBoxName;
    private Label labelInfo;
    private Panel panel1;
}