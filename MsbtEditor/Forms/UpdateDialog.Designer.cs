﻿namespace MsbtEditor;

partial class UpdateDialog
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        tableLayoutPanel = new TableLayoutPanel();
        richTextBox = new RichTextBox();
        buttonOk = new Button();
        tableLayoutPanel.SuspendLayout();
        SuspendLayout();
        // 
        // tableLayoutPanel
        // 
        tableLayoutPanel.ColumnCount = 1;
        tableLayoutPanel.ColumnStyles.Add(new ColumnStyle());
        tableLayoutPanel.Controls.Add(richTextBox, 0, 0);
        tableLayoutPanel.Controls.Add(buttonOk, 0, 1);
        tableLayoutPanel.Dock = DockStyle.Fill;
        tableLayoutPanel.Location = new Point(5, 5);
        tableLayoutPanel.Margin = new Padding(0);
        tableLayoutPanel.Name = "tableLayoutPanel";
        tableLayoutPanel.RowCount = 2;
        tableLayoutPanel.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
        tableLayoutPanel.RowStyles.Add(new RowStyle());
        tableLayoutPanel.Size = new Size(474, 251);
        tableLayoutPanel.TabIndex = 2;
        // 
        // richTextBox
        // 
        richTextBox.BackColor = Color.FromArgb(51, 51, 51);
        richTextBox.BorderStyle = BorderStyle.None;
        richTextBox.Dock = DockStyle.Fill;
        richTextBox.ForeColor = Color.FromArgb(212, 212, 212);
        richTextBox.Location = new Point(15, 15);
        richTextBox.Margin = new Padding(15);
        richTextBox.Name = "richTextBox";
        richTextBox.ReadOnly = true;
        richTextBox.ScrollBars = RichTextBoxScrollBars.Vertical;
        richTextBox.ShortcutsEnabled = false;
        richTextBox.Size = new Size(444, 192);
        richTextBox.TabIndex = 1;
        richTextBox.TabStop = false;
        richTextBox.Text = "";
        richTextBox.LinkClicked += RichTextBox_LinkClicked;
        // 
        // buttonOk
        // 
        buttonOk.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        buttonOk.FlatStyle = FlatStyle.Flat;
        buttonOk.Location = new Point(396, 225);
        buttonOk.Name = "buttonOk";
        buttonOk.Size = new Size(75, 23);
        buttonOk.TabIndex = 2;
        buttonOk.Text = "OK";
        buttonOk.UseVisualStyleBackColor = false;
        buttonOk.Click += ButtonOk_Click;
        // 
        // UpdateDialog
        // 
        AcceptButton = buttonOk;
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(51, 51, 51);
        ClientSize = new Size(484, 261);
        Controls.Add(tableLayoutPanel);
        ForeColor = Color.FromArgb(212, 212, 212);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "UpdateDialog";
        Padding = new Padding(5);
        ShowIcon = false;
        StartPosition = FormStartPosition.CenterParent;
        Text = "MSBT Editor Update";
        tableLayoutPanel.ResumeLayout(false);
        ResumeLayout(false);
    }

    #endregion

    private TableLayoutPanel tableLayoutPanel;
    private RichTextBox richTextBox;
    private Button buttonOk;
}