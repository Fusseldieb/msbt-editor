﻿namespace MsbtEditor;

partial class AboutDialog
{
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        tableLayoutPanel1 = new TableLayoutPanel();
        tableLayoutPanel2 = new TableLayoutPanel();
        richTextBox1 = new RichTextBox();
        buttonOk = new Button();
        pictureBox1 = new PictureBox();
        tableLayoutPanel1.SuspendLayout();
        tableLayoutPanel2.SuspendLayout();
        ((System.ComponentModel.ISupportInitialize)pictureBox1).BeginInit();
        SuspendLayout();
        // 
        // tableLayoutPanel1
        // 
        tableLayoutPanel1.ColumnCount = 2;
        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle());
        tableLayoutPanel1.ColumnStyles.Add(new ColumnStyle(SizeType.Percent, 100F));
        tableLayoutPanel1.Controls.Add(tableLayoutPanel2, 1, 0);
        tableLayoutPanel1.Controls.Add(pictureBox1, 0, 0);
        tableLayoutPanel1.Dock = DockStyle.Fill;
        tableLayoutPanel1.Location = new Point(5, 5);
        tableLayoutPanel1.Margin = new Padding(0);
        tableLayoutPanel1.Name = "tableLayoutPanel1";
        tableLayoutPanel1.RowCount = 1;
        tableLayoutPanel1.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
        tableLayoutPanel1.Size = new Size(524, 251);
        tableLayoutPanel1.TabIndex = 0;
        // 
        // tableLayoutPanel2
        // 
        tableLayoutPanel2.ColumnCount = 1;
        tableLayoutPanel2.ColumnStyles.Add(new ColumnStyle());
        tableLayoutPanel2.Controls.Add(richTextBox1, 0, 0);
        tableLayoutPanel2.Controls.Add(buttonOk, 0, 1);
        tableLayoutPanel2.Dock = DockStyle.Fill;
        tableLayoutPanel2.Location = new Point(160, 0);
        tableLayoutPanel2.Margin = new Padding(0);
        tableLayoutPanel2.Name = "tableLayoutPanel2";
        tableLayoutPanel2.RowCount = 2;
        tableLayoutPanel2.RowStyles.Add(new RowStyle(SizeType.Percent, 100F));
        tableLayoutPanel2.RowStyles.Add(new RowStyle());
        tableLayoutPanel2.Size = new Size(364, 251);
        tableLayoutPanel2.TabIndex = 2;
        // 
        // richTextBox1
        // 
        richTextBox1.BackColor = Color.FromArgb(51, 51, 51);
        richTextBox1.BorderStyle = BorderStyle.None;
        richTextBox1.Dock = DockStyle.Fill;
        richTextBox1.ForeColor = Color.FromArgb(212, 212, 212);
        richTextBox1.Location = new Point(15, 15);
        richTextBox1.Margin = new Padding(15);
        richTextBox1.Name = "richTextBox1";
        richTextBox1.ReadOnly = true;
        richTextBox1.ScrollBars = RichTextBoxScrollBars.Vertical;
        richTextBox1.ShortcutsEnabled = false;
        richTextBox1.Size = new Size(334, 192);
        richTextBox1.TabIndex = 1;
        richTextBox1.TabStop = false;
        richTextBox1.Text = "";
        // 
        // buttonOk
        // 
        buttonOk.Anchor = AnchorStyles.Top | AnchorStyles.Right;
        buttonOk.FlatStyle = FlatStyle.Flat;
        buttonOk.Location = new Point(286, 225);
        buttonOk.Name = "buttonOk";
        buttonOk.Size = new Size(75, 23);
        buttonOk.TabIndex = 2;
        buttonOk.Text = "OK";
        buttonOk.UseVisualStyleBackColor = false;
        buttonOk.Click += ButtonOk_Click;
        // 
        // pictureBox1
        // 
        pictureBox1.Anchor = AnchorStyles.Top;
        pictureBox1.BackColor = Color.Transparent;
        pictureBox1.ErrorImage = null;
        pictureBox1.Image = Properties.Resources.Logo;
        pictureBox1.InitialImage = null;
        pictureBox1.Location = new Point(20, 20);
        pictureBox1.Margin = new Padding(20);
        pictureBox1.Name = "pictureBox1";
        pictureBox1.Size = new Size(120, 120);
        pictureBox1.SizeMode = PictureBoxSizeMode.Zoom;
        pictureBox1.TabIndex = 3;
        pictureBox1.TabStop = false;
        // 
        // AboutDialog
        // 
        AcceptButton = buttonOk;
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(51, 51, 51);
        ClientSize = new Size(534, 261);
        Controls.Add(tableLayoutPanel1);
        ForeColor = Color.FromArgb(212, 212, 212);
        FormBorderStyle = FormBorderStyle.FixedDialog;
        MaximizeBox = false;
        MinimizeBox = false;
        Name = "AboutDialog";
        Padding = new Padding(5);
        ShowIcon = false;
        StartPosition = FormStartPosition.CenterParent;
        Text = "About MSBT Editor";
        tableLayoutPanel1.ResumeLayout(false);
        tableLayoutPanel2.ResumeLayout(false);
        ((System.ComponentModel.ISupportInitialize)pictureBox1).EndInit();
        ResumeLayout(false);
    }

    #endregion

    private TableLayoutPanel tableLayoutPanel1;
    private RichTextBox richTextBox1;
    private TableLayoutPanel tableLayoutPanel2;
    private Button buttonOk;
    private PictureBox pictureBox1;
}