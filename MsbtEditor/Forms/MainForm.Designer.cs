﻿namespace MsbtEditor;

partial class MainForm
{
    /// <summary>
    ///  Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    ///  Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
        if (disposing && (components != null))
        {
            components.Dispose();
        }
        base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    ///  Required method for Designer support - do not modify
    ///  the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
        System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
        blazorWebView = new Microsoft.AspNetCore.Components.WebView.WindowsForms.BlazorWebView();
        statusStrip = new StatusStrip();
        statusMessage = new ToolStripStatusLabel();
        statusMessage2 = new ToolStripStatusLabel();
        menuStrip = new MenuStrip();
        menuItemFile = new ToolStripMenuItem();
        toolStripLoadFiles = new ToolStripMenuItem();
        toolStripLoadFolder = new ToolStripMenuItem();
        toolStripMenuLoadFunctionMap = new ToolStripMenuItem();
        toolStripSeparator1 = new ToolStripSeparator();
        toolStripMenuLoadZstdDict = new ToolStripMenuItem();
        toolStripMenuUnloadZstdDict = new ToolStripMenuItem();
        toolStripSeparator2 = new ToolStripSeparator();
        toolStripMenuSave = new ToolStripMenuItem();
        toolStripMenuSaveAll = new ToolStripMenuItem();
        toolStripSeparator3 = new ToolStripSeparator();
        toolStripMenuAbout = new ToolStripMenuItem();
        toolStripMenuUpdate = new ToolStripMenuItem();
        toolStripMenuIExit = new ToolStripMenuItem();
        menuItemView = new ToolStripMenuItem();
        toolStripMenuZoomIn = new ToolStripMenuItem();
        toolStripMenuZoomOut = new ToolStripMenuItem();
        toolStripMenuZoomReset = new ToolStripMenuItem();
        toolStripSeparator5 = new ToolStripSeparator();
        toolStripMenuGlossary = new ToolStripMenuItem();
        toolStripSeparator4 = new ToolStripSeparator();
        toolStripMenuDevTools = new ToolStripMenuItem();
        toolStripMenuTaskManager = new ToolStripMenuItem();
        menuItemEditor = new ToolStripMenuItem();
        toolStripMenuWordWrap = new ToolStripMenuItem();
        toolStripMenuNewLine = new ToolStripMenuItem();
        toolStripMenuWhiteSpace = new ToolStripMenuItem();
        statusStrip.SuspendLayout();
        menuStrip.SuspendLayout();
        SuspendLayout();
        // 
        // blazorWebView
        // 
        blazorWebView.Dock = DockStyle.Fill;
        blazorWebView.Location = new Point(0, 24);
        blazorWebView.Margin = new Padding(0);
        blazorWebView.Name = "blazorWebView";
        blazorWebView.Size = new Size(1184, 715);
        blazorWebView.StartPath = "/";
        blazorWebView.TabIndex = 1;
        // 
        // statusStrip
        // 
        statusStrip.BackColor = Color.FromArgb(19, 124, 201);
        statusStrip.Items.AddRange(new ToolStripItem[] { statusMessage, statusMessage2 });
        statusStrip.Location = new Point(0, 739);
        statusStrip.Name = "statusStrip";
        statusStrip.Size = new Size(1184, 22);
        statusStrip.TabIndex = 2;
        // 
        // statusMessage
        // 
        statusMessage.ForeColor = Color.White;
        statusMessage.Name = "statusMessage";
        statusMessage.Size = new Size(10, 17);
        statusMessage.Text = " ";
        // 
        // statusMessage2
        // 
        statusMessage2.ForeColor = Color.White;
        statusMessage2.Name = "statusMessage2";
        statusMessage2.Size = new Size(1159, 17);
        statusMessage2.Spring = true;
        statusMessage2.TextAlign = ContentAlignment.MiddleRight;
        // 
        // menuStrip
        // 
        menuStrip.BackColor = Color.Transparent;
        menuStrip.Items.AddRange(new ToolStripItem[] { menuItemFile, menuItemView, menuItemEditor });
        menuStrip.Location = new Point(0, 0);
        menuStrip.Name = "menuStrip";
        menuStrip.Size = new Size(1184, 24);
        menuStrip.TabIndex = 3;
        // 
        // menuItemFile
        // 
        menuItemFile.BackColor = Color.FromArgb(51, 51, 51);
        menuItemFile.DropDownItems.AddRange(new ToolStripItem[] { toolStripLoadFiles, toolStripLoadFolder, toolStripMenuLoadFunctionMap, toolStripSeparator1, toolStripMenuLoadZstdDict, toolStripMenuUnloadZstdDict, toolStripSeparator2, toolStripMenuSave, toolStripMenuSaveAll, toolStripSeparator3, toolStripMenuAbout, toolStripMenuUpdate, toolStripMenuIExit });
        menuItemFile.ForeColor = Color.White;
        menuItemFile.Name = "menuItemFile";
        menuItemFile.Size = new Size(37, 20);
        menuItemFile.Text = "File";
        // 
        // toolStripLoadFiles
        // 
        toolStripLoadFiles.BackColor = Color.Transparent;
        toolStripLoadFiles.ForeColor = Color.White;
        toolStripLoadFiles.Name = "toolStripLoadFiles";
        toolStripLoadFiles.Size = new Size(199, 22);
        toolStripLoadFiles.Text = "Open Files...";
        toolStripLoadFiles.Click += ToolStripLoadFiles_Click;
        // 
        // toolStripLoadFolder
        // 
        toolStripLoadFolder.BackColor = Color.Transparent;
        toolStripLoadFolder.ForeColor = Color.White;
        toolStripLoadFolder.Name = "toolStripLoadFolder";
        toolStripLoadFolder.Size = new Size(199, 22);
        toolStripLoadFolder.Text = "Open Folder...";
        toolStripLoadFolder.Click += ToolStripLoadFolder_Click;
        // 
        // toolStripMenuLoadFunctionMap
        // 
        toolStripMenuLoadFunctionMap.BackColor = Color.Transparent;
        toolStripMenuLoadFunctionMap.ForeColor = Color.White;
        toolStripMenuLoadFunctionMap.Name = "toolStripMenuLoadFunctionMap";
        toolStripMenuLoadFunctionMap.Size = new Size(199, 22);
        toolStripMenuLoadFunctionMap.Text = "Load Function Map...";
        toolStripMenuLoadFunctionMap.Click += ToolStripMenuLoadFunctionMap_Click;
        // 
        // toolStripSeparator1
        // 
        toolStripSeparator1.Name = "toolStripSeparator1";
        toolStripSeparator1.Size = new Size(196, 6);
        // 
        // toolStripMenuLoadZstdDict
        // 
        toolStripMenuLoadZstdDict.BackColor = Color.Transparent;
        toolStripMenuLoadZstdDict.ForeColor = Color.White;
        toolStripMenuLoadZstdDict.Name = "toolStripMenuLoadZstdDict";
        toolStripMenuLoadZstdDict.Size = new Size(199, 22);
        toolStripMenuLoadZstdDict.Text = "Load ZSTD Dictionary...";
        toolStripMenuLoadZstdDict.Click += ToolStripMenuLoadZstdDict_Click;
        // 
        // toolStripMenuUnloadZstdDict
        // 
        toolStripMenuUnloadZstdDict.BackColor = Color.Transparent;
        toolStripMenuUnloadZstdDict.Enabled = false;
        toolStripMenuUnloadZstdDict.ForeColor = Color.White;
        toolStripMenuUnloadZstdDict.Name = "toolStripMenuUnloadZstdDict";
        toolStripMenuUnloadZstdDict.Size = new Size(199, 22);
        toolStripMenuUnloadZstdDict.Text = "Unload ZSTD Dictionary";
        toolStripMenuUnloadZstdDict.Click += ToolStripMenuUnloadZstdDict_Click;
        // 
        // toolStripSeparator2
        // 
        toolStripSeparator2.Name = "toolStripSeparator2";
        toolStripSeparator2.Size = new Size(196, 6);
        // 
        // toolStripMenuSave
        // 
        toolStripMenuSave.BackColor = Color.Transparent;
        toolStripMenuSave.ForeColor = Color.White;
        toolStripMenuSave.Name = "toolStripMenuSave";
        toolStripMenuSave.Size = new Size(199, 22);
        toolStripMenuSave.Text = "Save";
        toolStripMenuSave.Click += ToolStripMenuSave_Click;
        // 
        // toolStripMenuSaveAll
        // 
        toolStripMenuSaveAll.BackColor = Color.Transparent;
        toolStripMenuSaveAll.ForeColor = Color.White;
        toolStripMenuSaveAll.Name = "toolStripMenuSaveAll";
        toolStripMenuSaveAll.Size = new Size(199, 22);
        toolStripMenuSaveAll.Text = "Save All";
        toolStripMenuSaveAll.Click += ToolStripMenuSaveAll_Click;
        // 
        // toolStripSeparator3
        // 
        toolStripSeparator3.Name = "toolStripSeparator3";
        toolStripSeparator3.Size = new Size(196, 6);
        // 
        // toolStripMenuAbout
        // 
        toolStripMenuAbout.BackColor = Color.Transparent;
        toolStripMenuAbout.ForeColor = Color.White;
        toolStripMenuAbout.Name = "toolStripMenuAbout";
        toolStripMenuAbout.Size = new Size(199, 22);
        toolStripMenuAbout.Text = "About...";
        toolStripMenuAbout.Click += ToolStripMenuAbout_Click;
        // 
        // toolStripMenuUpdate
        // 
        toolStripMenuUpdate.BackColor = Color.Transparent;
        toolStripMenuUpdate.ForeColor = Color.White;
        toolStripMenuUpdate.Name = "toolStripMenuUpdate";
        toolStripMenuUpdate.Size = new Size(199, 22);
        toolStripMenuUpdate.Text = "Check for Update...";
        toolStripMenuUpdate.Click += ToolStripMenuUpdate_Click;
        // 
        // toolStripMenuIExit
        // 
        toolStripMenuIExit.BackColor = Color.Transparent;
        toolStripMenuIExit.ForeColor = Color.White;
        toolStripMenuIExit.Name = "toolStripMenuIExit";
        toolStripMenuIExit.Size = new Size(199, 22);
        toolStripMenuIExit.Text = "Exit";
        toolStripMenuIExit.Click += ToolStripMenuIExit_Click;
        // 
        // menuItemView
        // 
        menuItemView.BackColor = Color.FromArgb(51, 51, 51);
        menuItemView.DropDownItems.AddRange(new ToolStripItem[] { toolStripMenuZoomIn, toolStripMenuZoomOut, toolStripMenuZoomReset, toolStripSeparator5, toolStripMenuGlossary, toolStripSeparator4, toolStripMenuDevTools, toolStripMenuTaskManager });
        menuItemView.ForeColor = Color.White;
        menuItemView.Name = "menuItemView";
        menuItemView.Size = new Size(44, 20);
        menuItemView.Text = "View";
        // 
        // toolStripMenuZoomIn
        // 
        toolStripMenuZoomIn.BackColor = Color.Transparent;
        toolStripMenuZoomIn.ForeColor = Color.White;
        toolStripMenuZoomIn.Name = "toolStripMenuZoomIn";
        toolStripMenuZoomIn.ShortcutKeyDisplayString = "Ctrl++";
        toolStripMenuZoomIn.ShortcutKeys = Keys.Control | Keys.Oemplus;
        toolStripMenuZoomIn.Size = new Size(180, 22);
        toolStripMenuZoomIn.Text = "Zoom In";
        toolStripMenuZoomIn.Click += ToolStripMenuZoomIn_Click;
        // 
        // toolStripMenuZoomOut
        // 
        toolStripMenuZoomOut.BackColor = Color.Transparent;
        toolStripMenuZoomOut.ForeColor = Color.White;
        toolStripMenuZoomOut.Name = "toolStripMenuZoomOut";
        toolStripMenuZoomOut.ShortcutKeyDisplayString = "Ctrl+-";
        toolStripMenuZoomOut.ShortcutKeys = Keys.Control | Keys.OemMinus;
        toolStripMenuZoomOut.Size = new Size(180, 22);
        toolStripMenuZoomOut.Text = "Zoom Out";
        toolStripMenuZoomOut.Click += ToolStripMenuZoomOut_Click;
        // 
        // toolStripMenuZoomReset
        // 
        toolStripMenuZoomReset.BackColor = Color.Transparent;
        toolStripMenuZoomReset.Enabled = false;
        toolStripMenuZoomReset.ForeColor = Color.White;
        toolStripMenuZoomReset.Name = "toolStripMenuZoomReset";
        toolStripMenuZoomReset.ShortcutKeyDisplayString = "Ctrl+0";
        toolStripMenuZoomReset.ShortcutKeys = Keys.Control | Keys.NumPad0;
        toolStripMenuZoomReset.Size = new Size(180, 22);
        toolStripMenuZoomReset.Text = "Reset Zoom";
        toolStripMenuZoomReset.Click += ToolStripMenuZoomReset_Click;
        // 
        // toolStripSeparator5
        // 
        toolStripSeparator5.Name = "toolStripSeparator5";
        toolStripSeparator5.Size = new Size(177, 6);
        // 
        // toolStripMenuGlossary
        // 
        toolStripMenuGlossary.BackColor = Color.Transparent;
        toolStripMenuGlossary.ForeColor = Color.White;
        toolStripMenuGlossary.Name = "toolStripMenuGlossary";
        toolStripMenuGlossary.Size = new Size(180, 22);
        toolStripMenuGlossary.Text = "Toggle Function Glossary";
        toolStripMenuGlossary.Click += ToolStripMenuGlossary_Click;
        // 
        // toolStripSeparator4
        // 
        toolStripSeparator4.Name = "toolStripSeparator4";
        toolStripSeparator4.Size = new Size(177, 6);
        toolStripSeparator4.Visible = false;
        // 
        // toolStripMenuDevTools
        // 
        toolStripMenuDevTools.BackColor = Color.Transparent;
        toolStripMenuDevTools.Enabled = false;
        toolStripMenuDevTools.ForeColor = Color.White;
        toolStripMenuDevTools.Name = "toolStripMenuDevTools";
        toolStripMenuDevTools.Size = new Size(180, 22);
        toolStripMenuDevTools.Text = "Open Dev Tools";
        toolStripMenuDevTools.Visible = false;
        toolStripMenuDevTools.Click += ToolStripMenuDevTools_Click;
        // 
        // toolStripMenuTaskManager
        // 
        toolStripMenuTaskManager.BackColor = Color.Transparent;
        toolStripMenuTaskManager.Enabled = false;
        toolStripMenuTaskManager.ForeColor = Color.White;
        toolStripMenuTaskManager.Name = "toolStripMenuTaskManager";
        toolStripMenuTaskManager.Size = new Size(180, 22);
        toolStripMenuTaskManager.Text = "Open Task Manager";
        toolStripMenuTaskManager.Visible = false;
        toolStripMenuTaskManager.Click += ToolStripMenuTaskManager_Click;
        // 
        // menuItemEditor
        // 
        menuItemEditor.BackColor = Color.FromArgb(51, 51, 51);
        menuItemEditor.DropDownItems.AddRange(new ToolStripItem[] { toolStripMenuWordWrap, toolStripMenuNewLine, toolStripMenuWhiteSpace });
        menuItemEditor.ForeColor = Color.White;
        menuItemEditor.Name = "menuItemEditor";
        menuItemEditor.Size = new Size(50, 20);
        menuItemEditor.Text = "Editor";
        // 
        // toolStripMenuWordWrap
        // 
        toolStripMenuWordWrap.BackColor = Color.Transparent;
        toolStripMenuWordWrap.ForeColor = Color.White;
        toolStripMenuWordWrap.Name = "toolStripMenuWordWrap";
        toolStripMenuWordWrap.Size = new Size(173, 22);
        toolStripMenuWordWrap.Text = "Toggle Word Wrap";
        toolStripMenuWordWrap.Click += ToolStripMenuWordWrap_Click;
        // 
        // toolStripMenuNewLine
        // 
        toolStripMenuNewLine.BackColor = Color.Transparent;
        toolStripMenuNewLine.ForeColor = Color.White;
        toolStripMenuNewLine.Name = "toolStripMenuNewLine";
        toolStripMenuNewLine.Size = new Size(173, 22);
        toolStripMenuNewLine.Text = "Toggle New Line";
        toolStripMenuNewLine.Click += ToolStripMenuNewLine_Click;
        // 
        // toolStripMenuWhiteSpace
        // 
        toolStripMenuWhiteSpace.BackColor = Color.Transparent;
        toolStripMenuWhiteSpace.ForeColor = Color.White;
        toolStripMenuWhiteSpace.Name = "toolStripMenuWhiteSpace";
        toolStripMenuWhiteSpace.Size = new Size(173, 22);
        toolStripMenuWhiteSpace.Text = "Toggle Whitespace";
        toolStripMenuWhiteSpace.Click += ToolStripMenuWhiteSpace_Click;
        // 
        // MainForm
        // 
        AutoScaleDimensions = new SizeF(7F, 15F);
        AutoScaleMode = AutoScaleMode.Font;
        BackColor = Color.FromArgb(51, 51, 51);
        ClientSize = new Size(1184, 761);
        Controls.Add(blazorWebView);
        Controls.Add(menuStrip);
        Controls.Add(statusStrip);
        ForeColor = Color.FromArgb(212, 212, 212);
        Icon = (Icon)resources.GetObject("$this.Icon");
        MainMenuStrip = menuStrip;
        Margin = new Padding(2);
        Name = "MainForm";
        ShowIcon = false;
        Text = "MSBT Editor";
        FormClosing += MainForm_FormClosing;
        Load += MainForm_Load;
        statusStrip.ResumeLayout(false);
        statusStrip.PerformLayout();
        menuStrip.ResumeLayout(false);
        menuStrip.PerformLayout();
        ResumeLayout(false);
        PerformLayout();
    }

    #endregion

    private Microsoft.AspNetCore.Components.WebView.WindowsForms.BlazorWebView blazorWebView;
    private StatusStrip statusStrip;
    private ToolStripStatusLabel statusMessage;
    private ToolStripStatusLabel statusMessage2;
    private MenuStrip menuStrip;
    private ToolStripMenuItem menuItemFile;
    private ToolStripMenuItem toolStripLoadFiles;
    private ToolStripMenuItem toolStripLoadFolder;
    private ToolStripMenuItem toolStripMenuLoadFunctionMap;
    private ToolStripSeparator toolStripSeparator1;
    private ToolStripMenuItem toolStripMenuLoadZstdDict;
    private ToolStripMenuItem toolStripMenuUnloadZstdDict;
    private ToolStripSeparator toolStripSeparator2;
    private ToolStripMenuItem toolStripMenuSave;
    private ToolStripMenuItem toolStripMenuSaveAll;
    private ToolStripSeparator toolStripSeparator3;
    private ToolStripMenuItem toolStripMenuAbout;
    private ToolStripMenuItem toolStripMenuUpdate;
    private ToolStripMenuItem toolStripMenuIExit;
    private ToolStripMenuItem menuItemView;
    private ToolStripMenuItem toolStripMenuZoomIn;
    private ToolStripMenuItem toolStripMenuZoomOut;
    private ToolStripMenuItem toolStripMenuZoomReset;
    private ToolStripSeparator toolStripSeparator5;
    private ToolStripMenuItem toolStripMenuGlossary;
    private ToolStripSeparator toolStripSeparator4;
    private ToolStripMenuItem toolStripMenuDevTools;
    private ToolStripMenuItem toolStripMenuTaskManager;
    private ToolStripMenuItem menuItemEditor;
    private ToolStripMenuItem toolStripMenuWordWrap;
    private ToolStripMenuItem toolStripMenuNewLine;
    private ToolStripMenuItem toolStripMenuWhiteSpace;
}
