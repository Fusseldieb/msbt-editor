﻿using System.Reflection;

namespace MsbtEditor;

public partial class UpdateDialog : Form
{
    public UpdateDialog(UpdateInfo info)
    {
        InitializeComponent();

        richTextBox.SelectionProtected = true;
        richTextBox.Rtf = $$"""
                             {\rtf1\ansi
                             An update is available!\line\line

                             \b {{info.Title}} \b0\line
                             {{info.Description.Replace("\n", "\\line ")}}\line\line

                             Current Version: {{Assembly.GetExecutingAssembly().GetName().Version}}\line
                             New Version: {{info.Version}}\line
                             Download: {{info.Link}}
                             }
                             """;
    }

    private void ButtonOk_Click(object sender, EventArgs e) => Close();

    private void RichTextBox_LinkClicked(object sender, LinkClickedEventArgs e) => Close();
}