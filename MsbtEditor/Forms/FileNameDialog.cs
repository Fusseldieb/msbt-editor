﻿namespace MsbtEditor;

public partial class FileNameDialog : Form
{
    public FileNameDialog()
    {
        InitializeComponent();

        FileName = string.Empty;
        ActiveControl = textBoxName;
        DialogResult = DialogResult.Cancel;
    }

    public string FileName { get; private set; }

    private void TextBoxName_TextChanged(object sender, EventArgs e)
    {
        FileName = textBoxName.Text.Trim();
        buttonOk.Enabled = !string.IsNullOrWhiteSpace(FileName);
    }
}