﻿const _splitInstances = [];

export function setup(container, options) {
    const id = _splitInstances.length;
    const panels = container.querySelectorAll(':scope > .panel');

    options.onDragStart = function (p1, p2, gutter) {
        gutter.classList.add('active');
    };
    options.onDragEnd = function (p1, p2, gutter) {
        gutter.classList.remove('active');
    };

    const data = {
        instance: Split(panels, options),
        options: options
    };

    _splitInstances.push(data);
    return id;
}

export function collapse(id, index) {
    if (id >= _splitInstances.length) return;

    _splitInstances[id].instance.collapse(index);
}

export function expand(id, index) {
    if (id >= _splitInstances.length) return;

    const data = _splitInstances[id];
    const size = data.options.sizes[index];
    const minSize = getRelativeMinSize(data, index);
    const currentSizes = data.instance.getSizes();

    let total = 0;
    for (let i = 0; i < currentSizes.length; ++i) total += currentSizes[i];
    total += size - minSize;

    const newSizes = [];
    for (let i = 0; i < currentSizes.length; ++i) {
        if (i == index) newSizes.push(size * 100 / total);
        else newSizes.push(currentSizes[i] * 100 / total);
    }

    data.instance.setSizes(newSizes);
}

export function toggle(id, index) {
    if (id >= _splitInstances.length) return;

    const data = _splitInstances[id];
    const size = data.options.sizes[index];
    const minSize = getRelativeMinSize(data, index);
    const currentSizes = data.instance.getSizes();

    if (currentSizes[index] - minSize > 0.1) data.instance.collapse(index);
    else {
        let total = 0;
        for (let i = 0; i < currentSizes.length; ++i) total += currentSizes[i];
        total += size - minSize;

        const newSizes = [];
        for (let i = 0; i < currentSizes.length; ++i) {
            if (i == index) newSizes.push(size * 100 / total);
            else newSizes.push(currentSizes[i] * 100 / total);
        }

        data.instance.setSizes(newSizes);
    }
}

function getRelativeMinSize(data, index) {
    const maxSize = data.options.direction == 'vertical' ? data.instance.parent.clientHeight : data.instance.parent.clientWidth;
    return data.options.minSize[index] * 100 / maxSize;
}