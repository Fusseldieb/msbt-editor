﻿export function setup(container) {
    container.addEventListener('scroll', function () {
        container.classList.toggle('scrolled', container.scrollTop > 0);
    });
}