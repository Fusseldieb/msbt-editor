﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

internal static class Icons
{
    public static readonly MarkupString Add = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M5 12h14"/><path d="M12 5v14"/></svg>""");

    public static readonly MarkupString Save = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h11l5 5v11a2 2 0 0 1-2 2z"/><polyline points="17 21 17 13 7 13 7 21"/><polyline points="7 3 7 8 15 8"/></svg>""");

    public static readonly MarkupString SaveAll = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 4a2 2 0 0 1 2-2h10l4 4v10.2a2 2 0 0 1-2 1.8H8a2 2 0 0 1-2-2Z"/><path d="M10 2v4h6"/><path d="M18 18v-7h-8v7"/><path d="M18 22H4a2 2 0 0 1-2-2V6"/></svg>""");

    public static readonly MarkupString Edit = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M17 3a2.85 2.83 0 1 1 4 4L7.5 20.5 2 22l1.5-5.5Z"/><path d="m15 5 4 4"/></svg>""");

    public static readonly MarkupString Undo = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M9 14 4 9l5-5"/><path d="M4 9h10.5a5.5 5.5 0 0 1 5.5 5.5v0a5.5 5.5 0 0 1-5.5 5.5H11"/></svg>""");

    public static readonly MarkupString Remove = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M18 6 6 18"/><path d="m6 6 12 12"/></svg>""");

    public static readonly MarkupString Delete = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M3 6h18"/><path d="M19 6v14c0 1-1 2-2 2H7c-1 0-2-1-2-2V6"/><path d="M8 6V4c0-1 1-2 2-2h4c1 0 2 1 2 2v2"/><line x1="10" x2="10" y1="11" y2="17"/><line x1="14" x2="14" y1="11" y2="17"/></svg>""");

    public static readonly MarkupString ChevronUp = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="m18 15-6-6-6 6"/></svg>""");

    public static readonly MarkupString ChevronDown = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="m6 9 6 6 6-6"/></svg>""");

    public static readonly MarkupString ChevronLeft = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="m15 18-6-6 6-6"/></svg>""");

    public static readonly MarkupString ChevronRight = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="m9 18 6-6-6-6"/></svg>""");

    public static readonly MarkupString ArrowUp = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M8 6L12 2L16 6"/><path d="M12 2V22"/></svg>""");

    public static readonly MarkupString ArrowDown = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M8 18L12 22L16 18"/><path d="M12 2V22"/></svg>""");

    public static readonly MarkupString ArrowLeft = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M6 8L2 12L6 16"/><path d="M2 12H22"/></svg>""");

    public static readonly MarkupString ArrowRight = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M18 8L22 12L18 16"/><path d="M2 12H22"/></svg>""");

    public static readonly MarkupString ArrowUpDown = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="8 18 12 22 16 18"/><polyline points="8 6 12 2 16 6"/><line x1="12" x2="12" y1="2" y2="22"/></svg>""");

    public static readonly MarkupString ArrowLeftRight = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><polyline points="18 8 22 12 18 16"/><polyline points="6 8 2 12 6 16"/><line x1="2" x2="22" y1="12" y2="12"/></svg>""");

    public static readonly MarkupString Folder = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#bf9558" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20 20a2 2 0 0 0 2-2V8a2 2 0 0 0-2-2h-7.9a2 2 0 0 1-1.69-.9L9.6 3.9A2 2 0 0 0 7.93 3H4a2 2 0 0 0-2 2v13a2 2 0 0 0 2 2Z"/></svg>""");

    public static readonly MarkupString MsbtFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#599dd4" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M13 14l7 -0"/><path d="M16.5 14l0 8"/><path d="M7 22l-3 -0l0 -8l3 -0c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 -0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7 18l-3 -0"/><path d="M15.5 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M3.5 10l0 -8l4 4l4 -4l0 8"/></svg>""");

    public static readonly MarkupString UmsbtFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#c47b2f" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M13 14l7 -0"/><path d="M16.5 14l0 8"/><path d="M7 22l-3 0l0 -8l3 -0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7 18l-3 0"/><path d="M18 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M9 10l0 -8l3 4l3 -4l0 8"/><path d="M6 2l0 5.5c0 1.38 -1.12 2.5 -2.5 2.5c-1.38 0 -2.5 -1.12 -2.5 -2.5l0 -5.5"/></svg>""");

    public static readonly MarkupString SarcFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#54c8b0" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M19.5 21.464c-0.588 0.341 -1.272 0.536 -2 0.536c-2.208 -0 -4 -1.792 -4 -4c-0 -2.208 1.792 -4 4 -4c0.728 -0 1.412 0.195 2 0.536"/><path d="M4.5 14l0 8"/><path d="M4.5 14l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c0 -0 2 4 2 4"/><path d="M7.5 18l-3 -0"/><path d="M13 10l3.5 -8l3.5 8"/><path d="M19 8l-5 0"/><path d="M4 8c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/></svg>""");

    public static readonly MarkupString BymlFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#6c9858" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M15.5 14l0 8l5 0"/><path d="M3.5 22l0 -8l4 4l4 -4l0 8"/><path d="M13.5 2l3 4l3 -4"/><path d="M16.5 6l0 4"/><path d="M7.5 10l-3 0l0 -8l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7.5 6l-3 0"/></svg>""");

    public static readonly MarkupString AampFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#dcdbad" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M15.5 22l0 -8"/><path d="M15.5 18l3 0c1.104 0 2 -0.896 2 -2c0 -1.104 -0.896 -2 -2 -2l-3 0"/><path d="M3.5 22l0 -8l4 4l4 -4l0 8"/><path d="M14 10l3.5 -8l3.5 8"/><path d="M20 8l-5 0"/><path d="M3 10l3.5 -8l3.5 8"/><path d="M9 8l-5 0"/></svg>""");

    public static readonly MarkupString BcsvFile = new("""<svg xMlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#9fddfd" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M13 14l3.5 8l3.5 -8"/><path d="M4 20c0 1.104 1.12 2 2.5 2c1.38 0 2.5 -0.896 2.5 -2c0 -1.104 -0.831 -1.657 -2.5 -2c-1.927 -0.397 -2.5 -0.896 -2.5 -2c0 -1.104 1.12 -2 2.5 -2c1.38 0 2.5 0.896 2.5 2"/><path d="M19.5 9.464c-0.588 0.341 -1.272 0.536 -2 0.536c-2.208 0 -4 -1.792 -4 -4c0 -2.208 1.792 -4 4 -4c0.728 0 1.412 0.195 2 0.536"/><path d="M7.5 10l-3 0l-0 -8l3 0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M7.5 6l-3 0"/></svg>""");

    public static readonly MarkupString BmgFile = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#10a34e" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M20.5 13l2.5 0l0 3"/><path d="M22.905 13c-0.333 1.724 -1.508 3 -2.905 3c-1.656 0 -3 -1.792 -3 -4c-0 -2.208 1.344 -4 3 -4c0.947 -0 1.791 0.586 2.341 1.5"/><path d="M9 16l0 -8l2.5 4l2.5 -4l-0 8"/><path d="M4 16l-3 0l0 -8l3 -0c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2c1.104 0 2 0.896 2 2c0 1.104 -0.896 2 -2 2"/><path d="M4 12l-3 0"/></svg>""");

    public static readonly MarkupString ZipFile = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#0cc9c9" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><circle cx="15" cy="19" r="2"/><path d="M20.9 19.8A2 2 0 0 0 22 18V8a2 2 0 0 0-2-2h-7.9a2 2 0 0 1-1.69-.9L9.6 3.9A2 2 0 0 0 7.93 3H4a2 2 0 0 0-2 2v13a2 2 0 0 0 2 2h5.1"/><path d="M15 11v-1"/><path d="M15 17v-2"/></svg>""");

    public static readonly MarkupString FunctionMapFile = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#6b5bb3" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><rect width="18" height="18" x="3" y="3" rx="2" ry="2"/><path d="M9 17c2 0 2.8-1 2.8-2.8V10c0-2 1-3.3 3.2-3"/><path d="M9 11.2h5.7"/></svg>""");

    public static readonly MarkupString UnknownFile = new("""<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="#c5c5c5" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"><path d="M14.5 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V7.5L14.5 2z"/><path d="M10 10.3c.2-.4.5-.8.9-1a2.1 2.1 0 0 1 2.6.4c.3.4.5.8.5 1.3 0 1.3-2 2-2 2"/><path d="M12 17h.01"/></svg>""");
}