﻿using Microsoft.AspNetCore.Components;

namespace MsbtEditor;

public class UiAction
{
    public required string Name { get; init; }

    public required MarkupString Icon { get; init; }

    public required Func<Task> OnClick { get; init; }

    public bool Active { get; init; } = true;

    public static UiAction Separator => new()
    {
        Name = "separator",
        Icon = default,
        OnClick = () => Task.CompletedTask
    };
}