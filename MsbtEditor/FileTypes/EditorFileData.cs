﻿using BlazorMonaco.Editor;

namespace MsbtEditor;

public abstract class EditorFileData(StorageProvider storageProvider) : FileData(storageProvider)
{
    #region public properties
    public abstract string EditorLanguage { get; }

    public TextModel? EditorModel { get; set; }
    #endregion

    #region public methods
    public async Task LoadEditorContent()
    {
        if (EditorModel is null) return;

        var content = await InternalLoadEditorContent();
        await EditorModel.SetValue(content);
    }
    #endregion

    #region protected methods
    protected abstract Task<string> InternalLoadEditorContent();

    protected abstract Task InternalParseEditorContent(string content);

    protected override Task InternalLoad(Stream stream) => LoadEditorContent();

    protected override async Task InternalSave(Stream stream)
    {
        if (EditorModel is null) return;
        await InternalParseEditorContent(await EditorModel.GetValue(EndOfLinePreference.LF, false));
    }
    #endregion
}