﻿using NintendoTools.FileFormats.Bmg;

namespace MsbtEditor;

internal class BmgFileData(StorageProvider storageProvider, IMsbtSettings settings) : EditorFileData(storageProvider)
{
    #region private members
    private static readonly BmgFileParser Parser = new();
    private static readonly BmgFileCompiler Compiler = new();
    #endregion

    #region public properties
    public override string Type => "bmg";

    public override string EditorLanguage => "msbt";

    public BmgFile? Model { get; private set; }
    #endregion

    #region protected methods
    protected override Task InternalLoad(Stream stream) => Task.Run(async () =>
    {
        Model = Parser.Parse(stream);
        await base.InternalLoad(stream);
    });

    protected override Task InternalSave(Stream stream) => Task.Run(async () =>
    {
        if (Model is null) return;

        await base.InternalSave(stream);
        Compiler.Compile(Model, stream);
    });

    protected override async Task<string> InternalLoadEditorContent()
    {
        if (!IsInitialized) await Load();

        return TextConverter.DeserializeBmg(Model!, settings.FunctionMap);
    }

    protected override Task InternalParseEditorContent(string content) => Task.Run(() => Model = TextConverter.SerializeBmg(content, settings.FunctionMap));
    #endregion
}