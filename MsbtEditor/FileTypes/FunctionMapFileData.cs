﻿namespace MsbtEditor;

public class FunctionMapFileData(StorageProvider storageProvider) : EditorFileData(storageProvider)
{
    #region private members
    private string? _editorContent;
    #endregion

    #region public properties
    public override string Name => Path.GetFileNameWithoutExtension(FilePath);

    public override string Type => "mfm";

    public override string EditorLanguage => "mfm";

    public FunctionMap? Model { get; private set; }
    #endregion

    #region protected methods
    protected override async Task InternalLoad(Stream stream)
    {
        using var reader = new StreamReader(stream);
        var content = await reader.ReadToEndAsync();

        _editorContent = content;
        Model = FunctionMap.Parse(content);
        await base.InternalLoad(stream);
    }

    protected override async Task InternalSave(Stream stream)
    {
        if (_editorContent is null) return;

        await base.InternalSave(stream);
        await using var writer = new StreamWriter(stream);
        await writer.WriteAsync(_editorContent);
    }

    protected override async Task<string> InternalLoadEditorContent()
    {
        if (!IsInitialized) await Load();

        return _editorContent!;
    }

    protected override Task InternalParseEditorContent(string content) => Task.Run(() => Model = FunctionMap.Parse(content));
    #endregion
}