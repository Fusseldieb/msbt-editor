﻿using NintendoTools.FileFormats.Msbt;

namespace MsbtEditor;

internal class MsbtFileData(StorageProvider storageProvider, IMsbtSettings settings) : EditorFileData(storageProvider)
{
    #region private members
    private static readonly MsbtFileParser Parser = new();
    private static readonly MsbtFileCompiler Compiler = new();
    #endregion

    #region public properties
    public override string Type => "msbt";

    public override string EditorLanguage => "msbt";

    public MsbtFile? Model { get; private set; }
    #endregion

    #region protected methods
    protected override Task InternalLoad(Stream stream) => Task.Run(async () =>
    {
        Model = Parser.Parse(stream);
        await base.InternalLoad(stream);
    });

    protected override Task InternalSave(Stream stream) => Task.Run(async () =>
    {
        if (Model is null) return;

        await base.InternalSave(stream);
        Compiler.Compile(Model, stream);
    });

    protected override async Task<string> InternalLoadEditorContent()
    {
        if (!IsInitialized) await Load();

        return TextConverter.DeserializeMsbt(Model!, settings.FunctionMap);
    }

    protected override Task InternalParseEditorContent(string content) => Task.Run(() => Model = TextConverter.SerializeMsbt(content, settings.FunctionMap));
    #endregion
}