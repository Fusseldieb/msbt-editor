﻿namespace MsbtEditor;

public class AppSettings : IMsbtSettings, IZstdSettings
{
    public required FunctionMap FunctionMap { get; set; }

    public byte[]? ZstdDict { get; set; }

    public int ZstdCompressionLevel { get; set; }
}