﻿namespace MsbtEditor;

public interface IZstdSettings
{
    byte[]? ZstdDict { get; }

    int ZstdCompressionLevel { get; }
}