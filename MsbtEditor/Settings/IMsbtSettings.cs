﻿namespace MsbtEditor;

public interface IMsbtSettings
{
    FunctionMap FunctionMap { get; }
}