﻿namespace MsbtEditor;

public class UpdateInfo
{
    public required string Title { get; init; }

    public required string Description { get; init; }

    public required Version Version { get; init; }

    public required string Link { get; init; }
}