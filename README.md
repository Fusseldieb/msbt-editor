# MSBT Editor
An interactive editor for MSBT files. Allows opening, editing and saving MSBT files and provides syntax highlighting with customizable function maps.  
The **NintendoTools** library is used for processing Nintendo file formats: https://gitlab.com/AeonSake/nintendo-tools



## Features
- Opening, editing, and saving of MSBT and BMG files
- Editor syntax highlighting for MSBT files
- Opening and saving modified content of SARC, UMSBT and ZIP archives
- Opening and saving of compressed ZSTD files
- Support for ZSTD compression dictionaries
- Use of custom function maps


## MSBT Editor Syntax
To allow editing MSBT messages in an editor, the MSBT file structure is serialized into a special text format. This always contains general file metadata at the top of the text file, followed by the individual message entries. Special caution is advised when editing header/message metadata, especially attribute data (ATR1). This data is used by the game to map certain additional flags or arguments.

The text editor provides syntax highlighting for the MSBT serialization format to make editing as easy as possible.

### Header Format
The file header is delimited by three `%` symbols, containing metadata about the MSBT file. Do not edit this data unless you know what you are doing.

```ls
%%%
bigEndian: false
version: 3
encoding: utf-16
hasLBL1: true
labelGroups: 5
hasATR1: false
%%%
```

### Message Format
The header of a message is delimited by three `-` symbols, containing metadata about the message itself. It always contains a label value and additionally some attribute data. The actual text of the message, including the encoded function calls, follows directly after the message header.

```hbs
---
label: Talk_0015
---
{{resetAnim arg="[0,0]"}}{{anim type="LaughS_00"}}Hudson Construction's whole business is
built around {{color id="0"}}supporting{{color id="65535"}} our customers as
they {{color id="0"}}assemble their dream homes{{color id="65535"}}.
```

Functions and function arguments are parsed and mapped using function maps.


## Function Maps
MSBT contains special instructions for the rendering engine. Examples are changing text color/size, rendering ruby annotations, forcing a new dialog page, etc.
Beside the core system function group, function definitions vary between games. To better work with these functions, the editor supports the use of custom function maps.

By default, all function maps found in the `maps` directory next to the executable are loaded upon application start.

### Format
The format of custom MSBT function maps (.mfm) is as follows:

```yaml
[<group>, <type>] <name> #<description>
  <arg_datatype> <arg_name> #<arg_description>
  ...
...
```

As an example, the following definition is for the `ruby` system function:

```yaml
[0, 0] ruby #ruby annotation rendered above the normal text
  u16 charSpan #number of subsequent characters to render the annotation above (centered)
  string value #the annotation text
```

| Parameter         | Required | Description |
|-------------------|----------|-------------|
| `group`           | Yes      | Group index/ID of the function. |
| `type`            | Yes      | Type index/ID of the function. |
| `name`            | No       | An optional name for the function. If no name is specified, the function will be called `<group>:<type>`. |
| `description`     | No       | An optional description for the function. |
| `arg_datatype`    | Yes      | The data type of the function argument. See list below for valid types. |
| `arg_name`        | No       | An optional name for the function argument. If no name is specified, the argument will be called `arg<index+1>` |
| `arg_description` | No       | An optional description for the function argument. |

It is important to get the argument data types correct, otherwise mapping/parsing of MSBT files may fail or produce incorrect results. If a function has more arguments than specified, additional data will be shown as `otherArg="<hex_arg_value>"`.
During serialization/saving, found function arguments will be serialized in the same order as defined in the function map. Unspecified arguments will be appended in the order in which they were parsed. Unspecified argument values MUST be hex-string encoded data.

### Argument Data Types

| Type      | Alias                                       | Bytes | Description                   |
|-----------|---------------------------------------------|-------|-------------------------------|
| `bool`    | `boolean`                                   | 1     | Boolean                       |
| `u8`      | `uint8`, `byte`                             | 1     | Unsigned 8bit integer         |
| `s8`      | `i8`, `int8`, `sbyte`                       | 1     | Signed 8bit integer           |
| `u16`     | `uint16`, `ushort`                          | 2     | Unsigned 16bit integer        |
| `s16`     | `i16`, `int16`, `short`                     | 2     | Signed 16bit integer          |
| `u32`     | `uint32`, `uint`                            | 4     | Unsigned 32bit integer        |
| `s32`     | `i32`, `int32`, `int`                       | 4     | Signed 32bit integer          |
| `u64`     | `uint64`, `ulong`                           | 8     | Unsigned 64bit integer        |
| `s64`     | `i64`, `int64`, `long`                      | 8     | Signed 64bit integer          |
| `f32`     | `single`, `float`                           | 4     | 32bit float                   |
| `f64`     | `double`                                    | 8     | 64bit float                   |
| `str`     | `string`                                    | 2+n   | Text encoded in the format of the file; the first 2 bytes of the argument data are the length of the string |
| `nstr`    | `0str`, `nullStr`, `nstring`, `0string`, `nullString` | n+1   | Null-terminated text encoded in the format of the file; the last byte of the data is always `0x00` |
| `hex`     | `hexStr`, `hexString`                       | n     | Raw bytes as hex-encoded text |
| `<hex_value>` |                                         | n     | Data padding with the specified hex bytes |
| `_`       |                                             | n     | Data padding to align with the text encoding; always uses `0x00` as padding |
| `_<hex_value>` |                                        | n     | Data padding to align with the text encoding using the specified hex bytes |

Serialized/deserialized argument values depend on the endianness of the file and are automatically converted (see file header).

Data type declarations are case-insensitive. It is also possible to declare arguments as arrays. For this, the argument type must be followed by the size of the array in the format of `<datatype>[<array_size>]`.

The special discard data type `_` can be used to align argument data with the used text encoding. This argument can only be used as the very last argument in a function declaration. This is useful for e.g. files encoded in `UTF-16` (minimum of 2 bytes per character) but the argument data is only a single byte. The excess byte is removed by the game engine automatically but would show up in parsed text in the editor. This allows to "hide" additional bytes at the end of argument data (automatically aligns to the base width of the text format). Data discarded by this data type essentially removes any potentially existing data, so make sure, the data can actually be discarded.

### Type Ranges
Instead of specifying individual function types, type ranges allow combining several function definitions into a single declaration.

```yaml
[1, 0-2] example
  u16 arg1
```

The definition above will map to functions of group `1` with the type values `0`, `1` and `2`. The resulting function name in the editor is formatted as `<name>:<type>` or as `<group>:<type>` if no function name is defined.

### Type Discards
Sometimes function groups share the same argument definition across all types. To simplify the function declaration, *type discards* can be used instead of explicit type values. This also means that there can be only a single function definition for the entire function group.

```yaml
[1, _] example
  u16 arg1
```

The definition above will map to **ALL** functions of group `1`. The resulting function name in the editor is formatted as `<name>:<type>` or as `<group>:<type>` if no function name is defined.

### Value Maps
Value maps allow defining maps for argument and type values. This may be used for better readability or simply to validate value ranges. A value map can be defined at any point in the function map definition and is globally usable from all other function declarations within the function map. Note that data padding data types (literal or discard) cannot be used as value map data type.

```yaml
map <name> <datatype> #<description>
  <value> <value_name> #<value_description>
  ...
```

As an example, the following value map provides a list of names linked to a `u16` value.

```yaml
map nodeTypes u16 #a list of node types
  0 default
  1 start
  2 action
  3 decision
  4 end
```

All unspecified values in the map will result in a serialization error. Therefore, this can be used to validate editor data before compiling the final MSBT files.

| Parameter           | Required | Description |
|---------------------|----------|-------------|
| `name`              | Yes      | Name of the value map. |
| `datatype`          | No       | Data type of the value. Must be `u16` if used for function types. Defaults to `u16`. |
| `description`       | No       | An optional description for the function. |
| `value`             | Yes      | The actual value for the map. Value must be serializable into the data type of the map. |
| `value_name`        | No       | An optional name for value. If no name is specified, the name will be the same as the value. |
| `value_description` | No       | An optional description for the value. |

Value maps can be used for function arguments and function types:

```yaml
[1, {nodeTypes}] example #mapped function type
  u16 arg

[2, 3] example2
  {nodeTypes} arg #mapped argument value
```

Same as normal argument data types, mapped argument values can still be defined as arrays in the format of `{<map_name>}[<array_size>]`.


### Combining Types, Type Ranges, Type Maps and Type Discards
It is possible to combine fully qualified function declarations (group + type) with other flexible methods such as type ranges, type maps and type discards. Depending on the combination, the serialization approach may change. The following limitations apply:

- Only one fully qualified function declaration may share the name with a flexible function declaration
- Type ranges and maps must not overlap with the defined values
- Type discards are matched last after all other methods have been exhausted
- Order of declaration does not matter

The following example combines all methods:

```yaml
[1, 0] example
[1, 1] example2
[1, 2-3] example
[1, 4-5] example
[1, {exampleMap}] example
[1, _] example

map exampleMap u16
  6 type1
  7 type2
  8 type3
```

This will capture any of the following function strings:

- `{{example}}` -> mapped to `[1, 0]`, matched by the first declaration
- `{{example2}}` -> mapped to `[1, 1]`, matched by the second declaration
- `{{example:2}}` -> mapped to `[1, 2]`, matched by the first type range
- `{{example:4}}` -> mapped to `[1, 4]`, matched by the second type range
- `{{example:6}}` -> mapped to `[1, 6]`, matched by the type map
- `{{example:9}}` -> mapped to `[1, 9]`, matched by the type discard


## ZSTD Compression Dictionaries
Some games may use special ZSTD compression dictionaries. The editor also supports loading/saving files with custom compression dictionaries. For this load a file to use as compression dictionary via `File > Load ZSTD Dictionary`. After loading a dictionary, all ZSTD compressed files will be decompressed using this dictionary.

By default, files called `zs.zsdic` or `zs.dict` found next to the executable are loaded as ZSTD compression dictionary upon application start.
